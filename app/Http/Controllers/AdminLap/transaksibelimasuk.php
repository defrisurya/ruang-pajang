<?php

namespace App\Http\Controllers\AdminLap;

use App\Http\Controllers\Controller;
use App\Produk;
use App\ReservasiBeli;
use Illuminate\Http\Request;

class transaksibelimasuk extends Controller
{
    public function BeliMasuk()
    {
        $data = ReservasiBeli::with('detailbeli')->where('statuspengiriman', 'Dikirim')->get();
        $kirim = ReservasiBeli::with('detailbeli')->where('statuspengiriman', 'Terkirim')->get();
        // dd($data);
        return view('AdminLap.beli-masuk', compact('data', 'kirim'));
    }

    public function KonfirmasiKirimBeli($id)
    {
        $data = ReservasiBeli::with('detailbeli')->find($id);
        $data->update([
            'statuspengiriman' => 'Terkirim'
        ]);
        foreach ($data['detailbeli'] as $p) {
            $produk = Produk::find($p->id);
            $produk->update([
                'status' => 'terjual',
            ]);
        }
        toast('Lukisan Telah Dikirim', 'success');
        return redirect()->back();
    }
}
