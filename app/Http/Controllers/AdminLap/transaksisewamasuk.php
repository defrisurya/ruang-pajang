<?php

namespace App\Http\Controllers\AdminLap;

use App\Http\Controllers\Controller;
use App\Produk;
use App\ReservasiSewa;
use Illuminate\Http\Request;

class transaksisewamasuk extends Controller
{
    public function SewaMasuk()
    {
        $data = ReservasiSewa::with('detailsewa')->where('statuspengiriman', 'Dikirim')->get();
        $kirim = ReservasiSewa::with('detailsewa')->where('statuspengiriman', 'Terkirim')->get();
        // dd($data);
        return view('AdminLap.sewa-masuk', compact('data', 'kirim'));
    }

    public function KonfirmasiKirimSewa($id)
    {
        $data = ReservasiSewa::with('detailsewa')->find($id);
        $data->update([
            'statuspengiriman' => 'Terkirim'
        ]);
        foreach ($data['detailsewa'] as $p) {
            $produk = Produk::find($p->id);
            $produk->update([
                'status' => 'tersewa',
            ]);
        }
        toast('Lukisan Telah Dikirim', 'success');
        return redirect()->back();
    }
}
