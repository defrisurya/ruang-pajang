<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\Register\createCompany;
use App\Http\Requests\Front\Register\createPersonal;
use App\Providers\RouteServiceProvider;
use App\User;
use App\UserDetail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    // /**
    //  * Get a validator for an incoming registration request.
    //  *
    //  * @param  array  $data
    //  * @return \Illuminate\Contracts\Validation\Validator
    //  */
    // protected function validator(array $data)
    // {
    //     return Validator::make(
    //         $data, [
    //         'name' => ['required', 'string', 'max:255'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    //         'password' => ['required', 'string', 'min:8', 'confirmed'],
    //         'alamat' =>['required'],
    //         'telpon' =>['required'],
    //         'latitude' =>['required'],
    //         'langitude' =>['required'],
    //         'penanggungjawab' =>['required'],
    //         ]
    // );
    // }

    // /**
    //  * Create a new user instance after a valid registration.
    //  *
    //  * @param  array  $data
    //  * @return \App\User
    //  */
    // protected function create(array $data)
    // {
    //     $user = User::create([
    //         'name' => $data['name'],
    //         'email' => $data['email'],
    //         'password' => Hash::make($data['password']),
    //         'role' => 'customer',
    //     ]);

    //     UserDetail::create([
    //         'user_id' => $user->id,
    //         'alamat' => $data['alamat'],
    //         'telpon' => $data['telpon'],
    //         'latitude' => $data['latitude'],
    //         'longitude' => $data['longitude'],
    //         'penanggungjawab' => $data['penanggungjawab'],
    //     ]);
    // }
    protected function postpersonal(createPersonal $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 'customer',
        ]);

        UserDetail::create([
            'user_id' => $user->id,
            'alamat' => $request->alamat,
            'telpon' => $request->telpon,
            // 'latitude' => $request->latitude,
            // 'longitude' => $request->longitude,
            // 'penanggungjawab' => $request->penanggungjawab,
        ]);
        // return redirect()->route('home');
    }

    protected function postcompany(createCompany $request)
    {
        $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'role' => 'customer',
                ]);
        
                UserDetail::create([
                    'user_id' => $user->id,
                    'alamat' => $request->alamat,
                    'telpon' => $request->telpon,
                    'latitude' => $request->latitude,
                    'longitude' => $request->longitude,
                    'penanggungjawab' => $request->penanggungjawab,
                ]);
        // return redirect()->route('home');
    }

    public function registercompany()
    {
        return view('auth.registercompany');
    }
    public function registerpersonal()
    {
        return view('auth.registerpersonal');
    }
}
