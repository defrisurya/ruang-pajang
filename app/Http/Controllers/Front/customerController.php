<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\ReservasiBeli;
use App\ReservasiSewa;
use App\User;
use Facade\FlareClient\View;
use Illuminate\Http\Request;

class customerController extends Controller
{
    public function index()
    {
        if (auth()->user()->role == 'customercompany') {
            $belib = ReservasiBeli::where([
                ['statuspembayaran', '=', 'Terbayar'],
                ['user_id', '=', auth()->user()->id]
            ])->count();

            $belig = ReservasiBeli::where([
                ['statuspembayaran', '=', 'Gagal'],
                ['user_id', '=', auth()->user()->id]
            ])->count();
            $sewab = ReservasiSewa::where([
                ['statuspembayaran', 'Terbayar'],
                ['user_id', '=', auth()->user()->id]
            ])->count();
            $sewag = ReservasiSewa::where([
                ['statuspembayaran', 'Gagal'],
                ['user_id', '=', auth()->user()->id]
            ])->count();
            $belim = ReservasiBeli::where([
                ['statuspembayaran', 'Menunggu Pembayaran'],
                ['user_id', '=', auth()->user()->id]
            ])->count();
            // dd($belim);
            $sewam = ReservasiSewa::where([
                ['statuspembayaran', 'Menunggu Pembayaran'],
                ['user_id', '=', auth()->user()->id]
            ])->count();

            $totalb = $belib + $sewab;
            $totalg = $belig + $sewag;
            $totalm = $belim + $sewam;

            return view('Front.Dashboard.Personal.index', compact('totalb', 'totalg', 'totalm'));
        } else {
            $totalb = ReservasiBeli::where([
                ['statuspembayaran', 'Terbayar'],
                ['user_id', '=', auth()->user()->id]
            ])->count();
            $totalm = ReservasiBeli::where([
                ['statuspembayaran', 'Menunggu Pembayaran'],
                ['user_id', '=', auth()->user()->id]
            ])->count();
            $totalg = ReservasiBeli::where([
                ['statuspembayaran', 'Gagal'],
            ])->count();

            return view('Front.Dashboard.Personal.index', compact('totalb', 'totalg', 'totalm'));
        }
    }

    public function tbelipersonal()
    {
        $data = ReservasiBeli::with('detailbeli')->where('user_id', auth()->user()->id)->get();
        // dd($data->detailbeli);
        return view('Front.Dashboard.Personal.transaksibeli', compact('data'));
    }

    public function tsewa()
    {
        $data = ReservasiSewa::with('detailsewa')->where('user_id', auth()->user()->id)->get();
        // dd($data);
        return view('Front.Dashboard.Company.transaksisewa', compact('data'));
    }

    public function settingakunp()
    {
        $data = User::with('userdetail')->where('id', auth()->user()->id)->first();
        // dd($data);
        return view('Front.Dashboard.Personal.settingakun', compact('data'));
    }
}
