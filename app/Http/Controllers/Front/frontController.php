<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\KategoriProduk;
use App\Pelukis;
use App\Produk;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Response;
use Cookie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class frontController extends Controller
{
    public function index()
    {
        // dd(auth()->user());
        $kategoris = KategoriProduk::get();
        // dd($kategori);

        $collection = Produk::all()->count();
        $artist = Pelukis::all()->count();
        $client = User::where('role', 'customerpersonal')->count() + User::where('role', 'customercompany')->count();

        return view('Front.index', compact('kategoris', 'collection', 'artist', 'client'));
    }

    public function adminlap()
    {
        return view('AdminLap.dash_adminlap');
    }

    // public function management()
    // {
    //     return view('Management.dash_management');
    // }

    public function detail($id)
    {
        $produk = Produk::with(['pelukis', 'userdetail'])->where('id', $id)->first();
        // dd($produk);
        return view('Front.detail', compact('produk'));
    }

    public function postregister(Request $request)
    {
        $data = $request->all();
        // dd($data);
        if ($request->penanggungjawab) {
            $data['name'] = $request->penanggungjawab;
            $data['penanggungjawab'] = $request->name;

            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'role' => 'customercompany',
            ]);
            UserDetail::create([
                'user_id' => $user->id,
                'telpon' => $data['telpon'],
                'penanggungjawab' => $data['penanggungjawab'],
            ]);
            // $this->guard()->login($user);

            // $tawar = $request->session()->get('tawar');
            // if($tawar != null){
            //     return redirect()->route('keranjangbeli');
            // }
            return redirect()->route('login');
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => 'customerpersonal',
        ]);
        UserDetail::create([
            'user_id' => $user->id,
            'telpon' => $data['telpon']
        ]);

        // Auth::attempt($user);
        //     $tawar = $request->session()->get('tawar');
        //     if($tawar != null){
        //         return redirect()->route('keranjangbeli');
        //     }
        return redirect()->route('login');
    }

    public function artGalley()
    {
        $kategoris = KategoriProduk::get();
        $produks = Produk::with('pelukis')->where('status', 'tersedia')->get();
        // dd($produks);
        return view('Front.artgallery', compact('kategoris', 'produks'));
    }


    public function ourArtist()
    {
        $pelukis = Pelukis::with('produk')->get();
        // dd($pelukis);
        return view('Front.ourartist', compact('pelukis'));
    }

    public function artisdetail($id)
    {
        $pelukis = Pelukis::with('produk')->where('id', $id)->first();
        // dd($pelukis);
        return view('Front.detailartis', compact('pelukis'));
    }

    public function keranjangbeli(Request $request)
    {
        $cek = Produk::where('id', $request->produkid)->first();
        // dd($cek);
        if (isset($request->tawar)) {
            if ($request->tawar < $cek->hargafix) {
                alert()->info('Penawaran Belum Memenuhi', 'Ajukan Tawaran Lebih Tinggi');
                return redirect()->back();
            }

            // $request->session()->forget('tawar');

            if (!auth()->guard()->check()) {
                $request->session()->put('tawar', $request->tawar);
                return redirect()->route('login');
            }

            // Jika Tawar
            $carts = json_decode($request->cookie('rp-carts'), true);

            if ($carts && array_key_exists($request->produkid, $carts)) {
                alert()->info('Lukisan Telah Dikeranjang Anda', 'Silahkan Lakukan Transaksi');
                return redirect()->back();
            } else {
                $produk = Produk::with('pelukis')->find($request->produkid);
                $tawar = $request->session()->get('tawar');

                $carts[$request->produkid] = [
                    'qty' => '1',
                    'produk_id' => $produk->id,
                    'judul' => $produk->judul,
                    'foto' => $produk->foto,
                    'berat' => $produk->berat,
                    'price' => $request->tawar,
                    'media' => $produk->media,
                    'dimensi' => $produk->dimensi,
                    'pelukis' => $produk->pelukis['nama'],
                ];
            }

            $cookie = cookie('rp-carts', json_encode($carts), 2880);
            return redirect()->route('listkeranjangbeli')->cookie($cookie);
        }


        // Jika Tidak Tawar
        $carts = json_decode($request->cookie('rp-carts'), true);

        if ($carts && array_key_exists($request->produkid, $carts)) {
            alert()->info('Lukisan Telah Dikeranjang Anda', 'Silahkan Lakukan Transaksi');
            return redirect()->back();
        } else {
            $produk = Produk::with('pelukis')->find($request->produkid);
            // dd($produk);
            $carts[$request->produkid] = [
                'qty' => '1',
                'produk_id' => $produk->id,
                'judul' => $produk->judul,
                'foto' => $produk->foto,
                'berat' => $produk->berat,
                'price' => $produk->hargajual,
                'media' => $produk->media,
                'dimensi' => $produk->dimensi,
                'pelukis' => $produk->pelukis['nama'],
            ];
        }

        $cookie = cookie('rp-carts', json_encode($carts), 2880);

        return redirect()->route('listkeranjangbeli')->cookie($cookie);
        // $value = $request->session()->get('tawar');
        // dd($value);
    }

    public function keranjangsewa(Request $request, $id)
    {
        // dd($id);
        $cart = json_decode($request->cookie('rp-sewa'), true);
        // dd($cart);

        if ($cart && array_key_exists($request->produk_id, $cart)) {
            alert()->info('Lukisan Telah Dikeranjang Anda', 'Silahkan Lakukan Transaksi');
            return redirect()->back();
        } else {
            $produk = Produk::with('pelukis')->find($id);
            // dd($produk);
            $cart[$id] = [
                'qty' => '1',
                'produk_id' => $produk->id,
                'judul' => $produk->judul,
                'foto' => $produk->foto,
                'berat' => $produk->berat,
                'price' => $produk->hargasewa,
                'media' => $produk->media,
                'dimensi' => $produk->dimensi,
                'pelukis' => $produk->pelukis['nama'],
            ];
        }

        $cookie = cookie('rp-sewa', json_encode($cart), 2880);

        return redirect()->route('listkeranjangsewa')->cookie($cookie);
        // $value = $request->session()->get('tawar');
        // dd($value);
    }

    public function filterkategori($id)
    {
        $kategori = KategoriProduk::where('id', $id)->first();
        $data = Produk::where([
            ['kategori_id', $id],
            ['status', 'tersedia'],
        ])->get();

        return view('Front.filterkategori', compact('data', 'kategori'));
    }

    public function aboutus()
    {
        return view('Front.aboutus');
    }
}
