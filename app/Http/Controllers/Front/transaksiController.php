<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TransaksiSewa;
use App\Http\Requests\Front\TransaksiBeli;
use App\Ongkir;
use App\Voucher;
use App\Produk;
use App\ReservasiBeli;
use App\ReservasiDetailBeli;
use App\ReservasiDetailSewa;
use App\ReservasiSewa;
use App\UserDetail;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class transaksiController extends Controller
{

    public function listkeranjangbeli()
    {
        $carts = json_decode(request()->cookie('rp-carts'), true);
        if ($carts == null) {
            toast('Keranjang Pembelian Masih Kosong', 'info');
            return redirect()->route('welcome');
        }
        // dd($carts);
        foreach ($carts as $key => $item) {
            $produk = Produk::find($item['produk_id']);
            if ($produk['status'] == 'terjual') {
                unset($carts[$key]);
                $cookie = cookie('rp-carts', json_encode($carts), 2880);
                alert()->info('Produk Yang Dihapus Sudah Terjual', 'Pilih Produk Lainnya');
                return redirect()->route('listkeranjangbeli')->cookie($cookie);
            }
        }

        $provinsi = app('App\Http\Controllers\Front\rajaOngkirController')->get_province();
        // dd($provinsi);
        $carts = json_decode(request()->cookie('rp-carts'), true);
        // if ($carts == null) {
        //     toast('Keranjang Masih Kosong', 'info');
        //     return redirect()->route('welcome');
        // }
        $id = auth()->user()->id;
        $userdetail = UserDetail::where('user_id', $id)->first();
        // dd($userdetail);
        // dd($carts);
        // total berat
        $totalberat = 0;
        foreach ($carts as $r) {
            $totalberat += $r['berat'];
        }
        $berat = $totalberat * 1000;

        // total price
        $totalprice = 0;
        foreach ($carts as $r) {
            $totalprice += $r['price'];
        }
        $jml = count($carts);
        // dd(Str::random(15));

        return view('Front.keranjangbeli', compact('carts', 'userdetail', 'provinsi', 'berat', 'totalprice', 'jml'));
    }

    public function hapuscart($key)
    {
        $carts = json_decode(request()->cookie('rp-carts'), true);
        unset($carts[$key]);
        // dd($carts);
        $cookie = cookie('rp-carts', json_encode($carts), 2880);
        return redirect()->back()->cookie($cookie);
    }

    public function prosesbeli(TransaksiBeli $request)
    {

        // cek apakah sudah terjual produk

        DB::beginTransaction();
        try {

            $reservasibeli = ReservasiBeli::create([
                'user_id' => auth()->user()->id,
                'invoice' => Str::random(15),
                'tanggalreservasi' => date('Y-m-d H:i:s'),
                'alamatasal' => 'bantul',
                'alamatkirim' => $request->alamatkirim,
                'kurir' => $request->kurir,
                'ongkoskirim' => $request->ongkoskirim,
                'totalharga' => $request->totalharga,
                'totalqty' => $request->totalqty,
                'totalberat' => $request->totalberat,
                'statuspembayaran' => 'Menunggu Pembayaran',
                'statuspengiriman' => 'Diproses',
                'provinsi' => $request->provinsi,
                'namakota' => $request->namakota,
                'prov_id' => $request->province_id,
                'kota_id' => $request->kota_id,
            ]);

            $carts = json_decode(request()->cookie('rp-carts'), true);

            foreach ($carts as $row) {
                $produk = Produk::find($row['produk_id']);
                $produk->update([
                    'status' => 'terjual',
                ]);
                ReservasiDetailBeli::create([
                    'reservasi_beli_id' => $reservasibeli->id,
                    'produk_id' => $row['produk_id'],
                    'harga' => $row['price'],
                    'foto' => $row['foto'],
                    'dimensi' => $row['dimensi'],
                    'seniman' => $row['pelukis'],
                ]);
            }

            DB::commit();

            $carts = [];
            //KOSONGKAN DATA KERANJANG DI COOKIE
            $cookie = cookie('rp-carts', json_encode($carts), 2880);
            //REDIRECT KE HALAMAN FINISH TRANSAKSI
            alert()->success('Transaksi Sukses','Silahkan Melakukan Konfirmasi Pembayaran');
            return redirect()->route('transaksi.beli.p')->cookie($cookie);
        } catch (\Exception $e) {
            //JIKA TERJADI ERROR, MAKA ROLLBACK DATANYA
            DB::rollback();
            //DAN KEMBALI KE FORM TRANSAKSI SERTA MENAMPILKAN ERROR
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    /* Keranjang Sewa */
    public function listkeranjangsewa()
    {
        $cart = json_decode(request()->cookie('rp-sewa'), true);
        // dd($cart);
        if ($cart == null) {
            toast('Keranjang Sewa Masih Kosong', 'info');
            return redirect()->route('welcome');
        }
        // dd($cart);
        foreach ($cart as $key => $item) {
            $produk = Produk::find($item['produk_id']);
            if ($produk['status'] == 'tersewa') {
                unset($cart[$key]);
                $cookie = cookie('rp-sewa', json_encode($cart), 2880);
                alert()->info('Produk Yang Dihapus Sudah Tersewa', 'Pilih Produk Lainnya');
                return redirect()->route('listkeranjangsewa')->cookie($cookie);
            }
        }

        $ongkir = Ongkir::all();
        $vc = Voucher::all();

        // $provinsi = app('App\Http\Controllers\Front\rajaOngkirController')->get_province();
        // // dd($provinsi);
        // $cart = json_decode(request()->cookie('rp-sewa'), true);
        // // if ($carts == null) {
        // //     toast('Keranjang Masih Kosong', 'info');
        // //     return redirect()->route('welcome');
        // // }
        $id = auth()->user()->id;
        $userdetail = UserDetail::where('user_id', $id)->first();
        // // dd($userdetail);
        // // dd($carts);

        // total berat
        $totalberat = 0;
        foreach ($cart as $r) {
            $totalberat += $r['berat'];
        }
        $berat = $totalberat * 1000;

        // // total price
        $totalsewa = 0;
        // if ($vc->hargavoucher == true) {
        //     $hasil = $totalsewa - $vc->hargavoucher;
        // }
        foreach ($cart as $r) {
            $totalsewa += $r['price'];
        }

        $jml = count($cart);
        // // dd(Str::random(15));

        return view('Front.keranjangsewa', compact('ongkir', 'vc', 'cart', 'userdetail', 'berat', 'totalsewa', 'jml'));
    }

    public function hapus_cart($key)
    {
        $cart = json_decode(request()->cookie('rp-sewa'), true);
        unset($cart[$key]);
        // dd($carts);
        $cookie = cookie('rp-sewa', json_encode($cart), 2880);
        return redirect()->back()->cookie($cookie);
    }

    public function prosessewa(Request $request)
    {
        // dd($request->all());
        $a = $request->lamabulan * 30;
        // dd($lamamenyewa);
        $tanggal1 = $request->tglmulai;
        $tanggalakhir = date('Y-m-d', strtotime("+$a days", strtotime($tanggal1)));
        // dd($tanggalakhir);

        DB::beginTransaction();
        try {

            $reservasisewa = ReservasiSewa::create([
                'user_id' => auth()->user()->id,
                'invoice' => Str::random(15),
                'tgl_mulai' => $request->tglmulai,
                'tgl_akhir' => $tanggalakhir,
                'lamasewa' => $request->lamabulan,
                'totalvoucher' => $request->diskon,
                'alamatkirim' => $request->alamatkirim,
                'ongkoskirim' => $request->tarifsewa,
                'totalharga' => $request->totalfix,
                'statuspembayaran' => 'Menunggu Pembayaran',
                'statuspengiriman' => 'Diproses',
                'kota' => $request->kota,
            ]);

            $cart = json_decode(request()->cookie('rp-sewa'), true);

            foreach ($cart as $row) {
                $produk = Produk::find($row['produk_id']);
                $produk->update([
                    'status' => 'tersewa',
                ]);
                ReservasiDetailSewa::create([
                    'reservasi_sewa_id' => $reservasisewa->id,
                    'produk_id' => $row['produk_id'],
                    'harga' => $row['price'],
                    'foto' => $row['foto'],
                    'dimensi' => $row['dimensi'],
                    'seniman' => $row['pelukis'],
                ]);
            }

            DB::commit();

            $cart = [];
            //KOSONGKAN DATA KERANJANG DI COOKIE
            $cookie = cookie('rp-sewa', json_encode($cart), 2880);
            //REDIRECT KE HALAMAN FINISH TRANSAKSI
            alert()->success('Transaksi Sukses','Silahkan Melakukan Konfirmasi Pembayaran');
            return redirect()->route('transaksi.sewa')->cookie($cookie);
        } catch (\Exception $e) {
            //JIKA TERJADI ERROR, MAKA ROLLBACK DATANYA
            DB::rollback();
            //DAN KEMBALI KE FORM TRANSAKSI SERTA MENAMPILKAN ERROR
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }
}
