<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd(auth()->user());

        if(auth()->user()->role == 'manajemen')
        {
            return redirect()->route('manajemen');
        }
        if(auth()->user()->role == 'customerpersonal')
        {
            return redirect()->route('welcome');
        }
        if (auth()->user()->role == 'customercompany') {
            return redirect()->route('welcome');
        }
        if (auth()->user()->role == 'adminlap') {
            // dd('test');
            return redirect()->route('adminlap');
        }
        return view('home');
    }
} 
