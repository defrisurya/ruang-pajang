<?php

namespace App\Http\Controllers\Manajemen;

use App\Http\Controllers\Controller;
use App\ReservasiBeli;
use App\ReservasiSewa;
use Illuminate\Http\Request;

class manajemenController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function transaksibeli()
    {
        $data = ReservasiBeli::where('statuspembayaran', 'terbayar')->paginate(10);
        // dd($data);
        return view('Manajemen.transaksibeli', compact('data'));
    }

    public function detailtransaksibeli($id)
    {
        $data = ReservasiBeli::with(['detailbeli', 'user'])->find($id);
        // dd($data);
        return view('Manajemen.detailtransaksibeli', compact('data'));
    }

    public function transaksisewa()
    {
        $data = ReservasiSewa::where('statuspembayaran', 'terbayar')->paginate(10);
        // dd($data);
        return view('Manajemen.transaksisewa', compact('data'));
    }

    public function detailtransaksisewa($id)
    {
        $data = ReservasiSewa::with(['detailsewa', 'user'])->find($id);
        // dd($data);
        return view('Manajemen.detailtransaksisewa', compact('data'));
    }
}
