<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomController extends Controller
{
    public function index(Request $request)
    {
        $data = User::where('role', 'customerpersonal')->orwhere('role', 'customercompany')->orderBy('id', 'desc')->paginate(5);
        // dd($data);

        if ($request->cari) {
            if ($request->cari == "customerpersonal") {
                $data = User::where('role')->orderBy('id', 'desc')->paginate(5);
            } elseif ($request->cari == "customercompany") {
                $data = User::where('role')->orderBy('id', 'desc')->paginate(5);
            } else {
                $data = User::where(function ($data) use ($request) {
                    $data->where('name', 'like', "%$request->cari%");
                    $data->orWhere('email', 'like', "%$request->cari%");
                })->orderBy('id', 'desc')->paginate(5);
            }
        }
        return view('Superadmin.dataCustomer.dataCustomer', compact('data'));
    }

    public function updateindex($id)
    {
        $data = User::with('userdetail')->get()->find($id);
        // dd($data);
        return view('Superadmin.dataCustomer.detailCustomer', compact('data'));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        DB::table('user_details')->where('user_id', $id)->update([
            'telpon' => $request->telpon,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'premium' => $request->premium,
        ]);

        toast('Data Customer Berhasil Diperbaharui', 'success');
        return redirect()->route('customer');
    }
}
