<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Produk;
use App\ReservasiBeli;
use App\UserDetail;
use Illuminate\Http\Request;

class TransaksiBeli extends Controller
{
    public function index()
    {
        $data = ReservasiBeli::with(['detailbeli', 'user'])->get();
        // dd($data);
        return view('Superadmin.Transaksi.index', compact('data'));
    }

    public function DetailBeli($id)
    {
        $data = ReservasiBeli::with('detailbeli')->get()->find($id);
        return view('Superadmin.Transaksi.Detail-Beli', compact('data'));
    }

    public function konfirmasiBeli($id)
    {
        $data = ReservasiBeli::with(['detailbeli', 'user'])->find($id);

        $data->update([
            'statuspembayaran' => 'Terbayar',
            'statuspengiriman' => 'Dikirim'
        ]);
        foreach ($data['detailbeli'] as $p) {
            $produk = Produk::find($p->id);
            $produk->update([
                'status' => 'terjual',
                'user_detail_id' => $data->user->userdetail['id'],
            ]);
        }

        toast('Data Beli Berhasil Dikonfirmasi', 'success');
        return redirect('Superadmin/TransaksiBeli');
    }

    public function TolakBeli($id)
    {
        $data = ReservasiBeli::with('detailbeli')->find($id);
        $data->update([
            'statuspembayaran' => 'Gagal',
            'statuspengiriman' => 'Dibatalkan'
        ]);
        foreach ($data['detailbeli'] as $p) {
            $produk = Produk::find($p->id);
            $produk->update([
                'status' => 'tersedia',
            ]);
        }
        toast('Transaksi Pembelian Ditolak', 'success');
        return redirect()->back();
    }
}
