<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Produk;
use App\ReservasiSewa;
use Illuminate\Http\Request;

class TransaksiSewa extends Controller
{
    public function index()
    {
        $data = ReservasiSewa::with('detailsewa')->get();
        // dd($data);
        return view('Superadmin.Transaksi.sewa', compact('data'));
    }

    public function DetailSewa($id)
    {
        $data = ReservasiSewa::with('detailsewa')->find($id);
        return view('Superadmin.Transaksi.Detail-Sewa', compact('data'));
    }


    public function konfirmasiSewa($id)
    {
        $data = ReservasiSewa::with(['detailsewa', 'user'])->find($id);
        // dd($data->user->userdetail['id']);
        $data->update([
            'statuspembayaran' => 'Terbayar',
            'statuspengiriman' => 'Dikirim',
        ]);
        foreach ($data['detailsewa'] as $p) {
            $produk = Produk::find($p->id);
            // dd($produk);
            $produk->update([
                'status' => 'tersewa',
                'user_detail_id' => $data->user->userdetail['id'],
            ]);
        }

        toast('Data Sewa Berhasil Dikonfirmasi', 'success');
        return redirect('Superadmin/TransaksiSewa');
    }

    public function TolakSewa($id)
    {
        $data = ReservasiSewa::with('detailsewa')->find($id);
        $data->update([
            'statuspembayaran' => 'Gagal',
            'statuspengiriman' => 'Dibatalkan'
        ]);
        foreach ($data['detailsewa'] as $p) {
            $produk = Produk::find($p->id);
            $produk->update([
                'status' => 'tersedia',
            ]);
        }
        toast('Transaksi Sewa Ditolak', 'success');

        return redirect()->back();
    }
}
