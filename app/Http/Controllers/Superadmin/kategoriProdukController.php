<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Superadmin\KategoriProduk\KategoriProdukCreate;
use App\Http\Requests\Superadmin\KategoriProduk\KategoriProdukUpdate;
use App\KategoriProduk;
use App\Produk;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class kategoriProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = KategoriProduk::paginate(10);
        return view('Superadmin.KategoriProduk.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KategoriProdukCreate $request)
    {
        $data = $request->all();
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'kategori' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/Kategori/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        KategoriProduk::create($data);
        toast('Data Kategori Di Tambahkan','success');
        return redirect()->route('kategoriproduk.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(KategoriProduk $kategoriproduk)
    {
        return view('Superadmin.KategoriProduk.edit', compact('kategoriproduk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KategoriProdukUpdate $request, KategoriProduk $kategoriproduk)
    {
        $data = $request->all();
        $data['foto'] = $kategoriproduk->foto;
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'kategori' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/Kategori/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        $kategoriproduk->update($data);
        toast('Data Kategori Di Update','success');
        return redirect()->route('kategoriproduk.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(KategoriProduk $kategoriproduk)
    {
        $cek = Produk::where('kategori_id', $kategoriproduk->id)->first();
        if(isset($cek)){
            alert()->info('Info','Data Kategori Di Gunakan Pada Produk');
            return redirect()->route('kategoriproduk.index');
        }
        $kategoriproduk->delete();
        toast('Data Kategori Di Hapus','error');
        return redirect()->route('kategoriproduk.index');
    }
}
