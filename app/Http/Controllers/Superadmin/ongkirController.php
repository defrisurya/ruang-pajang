<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Ongkir\ongkirRequestCU;
use App\Ongkir;
use Illuminate\Http\Request;

class ongkirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Ongkir::paginate(10);
        return view('Superadmin.Ongkir.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ongkirRequestCU $request)
    {
        $data = $request->all();
        Ongkir::create($data);
        toast('Data Ongkir Di Tambahkan', 'success');
        return redirect()->route('ongkir.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ongkir $ongkir)
    {
        return view('Superadmin.Ongkir.edit', compact('ongkir'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ongkirRequestCU $request, Ongkir $ongkir)
    {
        $data = $request->all();
        // dd($data);
        $ongkir->update($data);
        toast('Data Ongkir Di Update', 'success');
        return redirect()->route('ongkir.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ongkir $ongkir)
    {
        $ongkir->delete();
        toast('Data Ongkir Di Hapus', 'error');
        return redirect()->route('ongkir.index');
    }
}
