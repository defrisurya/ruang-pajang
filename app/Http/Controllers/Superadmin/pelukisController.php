<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Superadmin\Pelukis\pelukisCreate;
use App\Http\Requests\Superadmin\Pelukis\pelukisUpdate;
use App\Pelukis;
use App\Produk;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class pelukisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pelukis::paginate(10);
        return view('Superadmin.Pelukis.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Superadmin.Pelukis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(pelukisCreate $request)
    {
        $data = $request->all();
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'kategori' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/Kategori/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        Pelukis::create($data);
        toast('Data Pelukis Di Tambahkan','success');
        return redirect()->route('pelukis.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pelukis $pelukis)
    {
        return view('Superadmin.Pelukis.edit', compact('pelukis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(pelukisUpdate $request, Pelukis $pelukis)
    {
        $data = $request->all();
        $data['foto'] = $pelukis->foto;
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'kategori' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/Kategori/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        $pelukis->update($data);
        toast('Data Pelukis Di Update','success');
        return redirect()->route('pelukis.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pelukis $pelukis)
    {
        $cek = Produk::where('pelukis_id', $pelukis->id)->first();
        if(isset($cek)){
            alert()->info('Info','Data Pelukis Di Gunakan Pada Produk');
            return redirect()->route('pelukis.index');
        }
        $pelukis->delete();
        toast('Data Pelukis Di Hapus','error');
        return redirect()->route('pelukis.index');
    }
}
