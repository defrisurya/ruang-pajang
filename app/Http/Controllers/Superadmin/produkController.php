<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Superadmin\Produk\createProduk;
use App\Http\Requests\Superadmin\Produk\updateProduk;
use App\KategoriProduk;
use App\Pelukis;
use App\Produk;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class produkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Produk::with(['pelukis'])->paginate(10);
        // dd($data);
        return view('Superadmin.Produk.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = KategoriProduk::get();
        $pelukis = Pelukis::get();
        return view('Superadmin.Produk.create', compact('kategori', 'pelukis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createProduk $request)
    {
        $data = $request->all();
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/Produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        // $data['status'] = 'tersedia';
        Produk::create($data);
        toast('Data Produk Di Tambahkan','success');
        return redirect()->route('produk.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Produk $produk)
    {
        $kategori = KategoriProduk::get();
        $pelukis = Pelukis::get();
        return view('Superadmin.Produk.edit', compact('produk', 'kategori', 'pelukis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateProduk $request, Produk $produk)
    {
        $data = $request->all();
        $data['foto'] = $produk->foto;
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/Produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        $produk->update($data);
        toast('Data Produk Di Update','success');
        return redirect()->route('produk.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
