<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Superadmin\Voucher\vocherRequestCU;
use App\Voucher;
use Illuminate\Http\Request;

class voucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Voucher::paginate(10);
        return view('Superadmin.Voucher.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(vocherRequestCU $request)
    {
        // $cek = Voucher::count();
        // if ($cek >= 1) {
        //     toast('Data Voucher Hanya Bisa 1', 'info');
        //     return redirect()->route('voucher.index');
        // }
        $data = $request->all();
        Voucher::create($data);
        toast('Data Voucher Berhasil Ditambah', 'success');
        return redirect()->route('voucher.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Voucher $voucher)
    {
        return view('Superadmin.Voucher.edit', compact('voucher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(vocherRequestCU $request, Voucher $voucher)
    {
        $data = $request->all();
        $voucher->update($data);
        toast('Data Voucher Berhasil DiEdit', 'success');
        return redirect()->route('voucher.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voucher $voucher)
    {
        $voucher->delete();
        toast('Data Voucher Berhasil Di Hapus', 'error');
        return redirect()->route('voucher.index');
    }
}
