<?php

namespace App\Http\Requests\Superadmin\KategoriProduk;

use Illuminate\Foundation\Http\FormRequest;

class KategoriProdukUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'namakategori' => 'required',
            'foto' => 'nullable',
        ];
    }
}
