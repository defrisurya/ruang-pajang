<?php

namespace App\Http\Requests\Superadmin\Produk;

use Illuminate\Foundation\Http\FormRequest;

class updateProduk extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pelukis_id' => 'required',
            'kategori_id' => 'required',
            'foto' => 'nullable|mimes:jpeg,png,jpg',
            'judul' => 'required',
            'tahunpembuatan' => 'required',
            'aliranlukis' => 'required',
            'hargasewa' => 'required',
            'hargajual' => 'required',
            'hargafix' => 'required',
            'dimensi' => 'required',
            'media' => 'required',
            'status' => 'required',
            'deskripsi' => 'required',
            'berat' => 'required',
        ];
    }
}
