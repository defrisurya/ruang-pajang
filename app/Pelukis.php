<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelukis extends Model
{
    protected $fillable = [
        'nama',
        'foto',
        'tahunkarya',
        'aliranlukis',
        'deskripsi',
        'deskripsipengalaman',
    ];

    public function produk()
    {
        return $this->hasMany(Produk::class);
    }
}
