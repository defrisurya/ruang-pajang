<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $fillable = [
        'kategori_id',
        'pelukis_id',
        'user_detail_id',
        'foto',
        'judul',
        'tahunpembuatan',
        'aliranlukis',
        'hargasewa',
        'hargajual',
        'hargafix',
        'dimensi',
        'media',
        'status',
        'deskripsi',
        'berat',
    ];

    public function kategori()
    {
        return $this->belongsTo(KategoriProduk::class);
    }

    public function pelukis()
    {
        return $this->belongsTo(Pelukis::class);
    }

    public function ongkir()
    {
        return $this->belongsTo(Ongkir::class);
    }

    static function list_produk()
    {
        $data = Produk::all();
        return $data;
    }

    public function userdetail()
    {
        return $this->belongsTo(UserDetail::class, 'user_detail_id', 'id');
    }

    static function tambah_produk($foto, $judul, $thn, $aliran, $hargasewa, $hargajual, $hargafix, $dimensi, $media, $status, $deskripsi, $berat)
    {
        Produk::create([
            'foto' => $foto,
            'judul' => $judul,
            'tahunpembuatan' => $thn,
            'aliranlukis' => $aliran,
            'hargasewa' => $hargasewa,
            'hargajual' => $hargajual,
            'hargafix' => $hargafix,
            'dimensi' => $dimensi,
            'media' => $media,
            'status' => $status,
            'deskripsi' => $deskripsi,
            'berat' => $berat,
        ]);
    }

    static function detail_produk($id_produk)
    {
        $data = Produk::where("id", $id_produk)->first();
        return $data;
    }
}
