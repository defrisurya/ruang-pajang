<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservasiDetailSewa extends Model
{
    protected $table = 'reservasi_detail_sewas';
    protected $fillable = [
        'reservasi_sewa_id',
        'produk_id',
        'harga',
        'foto',
        'dimensi',
        'seniman',
    ];
}
