<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservasiSewa extends Model
{
    protected $table = 'reservasi_sewas';
    protected $fillable = [
        'user_id',
        'tgl_mulai',
        'tgl_akhir',
        'lamasewa',
        'totalvoucher',
        'alamatkirim',
        'ongkoskirim',
        'totalharga',
        'statuspembayaran',
        'statuspengiriman',
        'invoice',
        'kota',
    ];


    public function detailsewa()
    {
        return $this->hasMany(ReservasiDetailSewa::class, 'reservasi_sewa_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
