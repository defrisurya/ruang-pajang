<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserDetail extends Model
{
    protected $fillable = ['user_id', 'alamat', 'telpon', 'latitude', 'longitude', 'penanggungjawab', 'premium'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
