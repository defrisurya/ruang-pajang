<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasiBelisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservasi_belis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->text('alamatasal');
            $table->text('alamatkirim');
            $table->string('kurir');
            $table->string('ongkoskirim');
            $table->string('totalharga');
            $table->integer('totalqty');
            $table->string('totalberat');
            $table->enum('statuspembayaran', ['Menunggu Pembayaran', 'Terbayar', 'Gagal']);
            $table->enum('statuspengiriman', ['Diproses', 'Dikirim', 'Terkirim']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservasi_belis');
    }
}
