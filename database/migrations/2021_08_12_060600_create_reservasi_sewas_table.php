<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasiSewasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservasi_sewas', function (Blueprint $table) {
            $table->id();
            $table->string('invoice');
            $table->foreignId('user_id');
            $table->date('tgl_mulai');
            $table->date('tgl_akhir');
            $table->string('lamasewa');
            $table->string('totalvoucher')->nullable();
            $table->text('alamatkirim');
            $table->string('ongkoskirim');
            $table->string('totalharga');
            $table->string('kota');
            $table->enum('statuspembayaran', ['Menunggu Pembayaran', 'Terbayar', 'Gagal']);
            $table->enum('statuspengiriman', ['Diproses', 'Dikirim', 'Terkirim']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservasi_sewas');
    }
}
