<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProvinsiOnReservasibeli extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservasi_belis', function (Blueprint $table) {
            $table->string('provinsi');
            $table->string('namakota');
            $table->string('prov_id');
            $table->string('kota_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservasi_belis', function (Blueprint $table) {
            $table->dropColumn('provinsi');
            $table->dropColumn('namakota');
            $table->dropColumn('prov_id');
            $table->dropColumn('kota_id');
        });
    }
}
