<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInvoiceOnReservasibeli extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservasi_belis', function (Blueprint $table) {
            $table->string('invoice');
            $table->date('tanggalreservasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservasi_belis', function (Blueprint $table) {
            $table->dropColumn('invoice');
            $table->dropColumn('tanggalreservasi');
        });
    }
}
