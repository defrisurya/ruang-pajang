<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFotoDllOnReservasidetailbeli extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservasi_detail_belis', function (Blueprint $table) {
            $table->string('foto');
            $table->string('dimensi');
            // $table->string('harga');
            $table->string('seniman');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservasi_detail_belis', function (Blueprint $table) {
            $table->dropColumn('foto');
            $table->dropColumn('dimensi');
            // $table->dropColumn('harga');
            $table->dropColumn('seniman');
        });
    }
}
