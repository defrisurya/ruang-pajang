<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeskripsiPengalamanOnPelukis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pelukis', function (Blueprint $table) {
            $table->text('deskripsipengalaman');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pelukis', function (Blueprint $table) {
            $table->dropColumn('deskripsipengalaman');
        });
    }
}
