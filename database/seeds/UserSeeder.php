<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => 'admin@mail.com',
            'role' => 'superadmin',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'Admin Lapangan',
            'email' => 'adminlap@mail.com',
            'role' => 'adminlap',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => 'manajemen@mail.com',
            'role' => 'manajemen',
            'password' => Hash::make('password'),
        ]);
    }
}
