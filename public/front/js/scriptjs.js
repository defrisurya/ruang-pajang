const mediaQuery = window.matchMedia('(max-width: 600px)')
if (mediaQuery.matches) {
    var element = document.getElementById("sidebar");
    element.classList.remove("active-nav");
    var element = document.getElementById("container");
    element.classList.remove("active-cont");
}
var menu_btn = document.querySelector("#menu-btn");
    var sidebar = document.querySelector("#sidebar");
    var container = document.querySelector(".my-container");
    menu_btn.addEventListener("click", () => {
        sidebar.classList.toggle("active-nav");
        container.classList.toggle("active-cont");
    });
