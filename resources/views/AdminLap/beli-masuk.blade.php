@extends('layouts.adminlap')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <h4>Data Pengiriman Pembelian</h4>
            </div>
            <div class="card-body">
                <table class="card-table table table-hover text-center">
                    <thead>
                        <tr>
                            <th scope="col">
                                No
                            </th>
                            <th scope="col">
                                Invoice
                            </th>
                            <th scope="col">
                                Customer
                            </th>
                            <th scope="col">
                                Ekspedisi
                            </th>
                            <th scope="col">
                                Status
                            </th>
                            <th scope="col">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @forelse ($data as $item)
                            <tr>
                                <th scope="row">{{ $no++ }}</th>
                                <td>{{ $item->invoice }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->kurir }}</td>
                                <td>
                                    <div class="status" style="color: red;">
                                        {{ $item->statuspengiriman }}
                                    </div>
                                </td>
                                <td>
                                    <p>
                                        <a class="btn btn-success" href="{{ route('konfirmasikirimBeli', $item->id) }}">
                                            Konfirmasi
                                        </a>
                                        <a class="btn btn-primary" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            Detail
                                        </a>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <div class="collapse" id="collapseExample">
                                        <div class="card card-body">
                                            <table class="table text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Lukisan</th>
                                                        <th scope="col">Harga</th>
                                                        <th scope="col">Total QTY</th>
                                                        <th scope="col">Total Berat</th>
                                                        <th scope="col">Alamat Asal</th>
                                                        <th scope="col">Alamat Kirim</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse ($data  as $items)
                                                    <tr>
                                                        @forelse ($item->detailbeli  as $row)
                                                        <td>
                                                            <img src="{{ asset($row->foto) }}" class="img-fluid" width="150px" alt="">
                                                        </td>
                                                        @empty

                                                        @endforelse
                                                        <td>Rp {{ number_format($items->totalharga) }}</td>
                                                        <td>{{ $items->totalqty }}</td>
                                                        <td>{{ $items->totalberat }} Gram</td>
                                                        <td>{{ $items->alamatasal }}</td>
                                                        <td>{{ $items->alamatkirim }}</td>
                                                    </tr>
                                                    @empty

                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-12 card">
            <div class="card-header">
                <h4>Data Pengiriman Pembelian (Terkirim)</h4>
            </div>
            <div class="card-body">
                <table class="card-table table table-hover text-center">
                    <thead>
                        <tr>
                            <th scope="col">
                                No
                            </th>
                            <th scope="col">
                                Invoice
                            </th>
                            <th scope="col">
                                Customer
                            </th>
                            <th scope="col">
                                Ekspedisi
                            </th>
                            <th scope="col">
                                Status
                            </th>
                            <th scope="col">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @forelse ($kirim as $item)
                            <tr>
                                <th scope="row">{{ $no++ }}</th>
                                <td>{{ $item->invoice }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->kurir }}</td>
                                <td>
                                    <div class="status" style="color: red;">
                                        {{ $item->statuspengiriman }}
                                    </div>
                                </td>
                                <td>
                                    <p>
                                        <a class="btn btn-primary" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            Detail
                                        </a>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <div class="collapse" id="collapseExample">
                                        <div class="card card-body">
                                            <table class="table text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Lukisan</th>
                                                        <th scope="col">Harga</th>
                                                        <th scope="col">Total QTY</th>
                                                        <th scope="col">Total Berat</th>
                                                        <th scope="col">Alamat Asal</th>
                                                        <th scope="col">Alamat Kirim</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse ($kirim  as $items)
                                                    <tr>
                                                        @forelse ($item->detailbeli  as $row)
                                                        <td>
                                                            <img src="{{ asset($row->foto) }}" class="img-fluid" width="150px" alt="">
                                                        </td>
                                                        @empty

                                                        @endforelse
                                                        <td>Rp {{ number_format($items->totalharga) }}</td>
                                                        <td>{{ $items->totalqty }}</td>
                                                        <td>{{ $items->totalberat }} Gram</td>
                                                        <td>{{ $items->alamatasal }}</td>
                                                        <td>{{ $items->alamatkirim }}</td>
                                                    </tr>
                                                    @empty

                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
