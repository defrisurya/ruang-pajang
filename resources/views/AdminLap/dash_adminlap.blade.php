@extends('layouts.adminlap')

@section('content')
<!-- Main Content -->
@include('sweetalert::alert')
<div class="main-content">
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-success">
                    <i class="fas fa-clipboard-list"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4 style="color: black">Transaksi Pembelian</h4>
                    </div>
                    <div class="card-body">
                        {{ App\ReservasiBeli::where('statuspengiriman', 'Dikirim')->count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="fas fa-clipboard-list"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4 style="color: black">Transaksi Penyewaan</h4>
                    </div>
                    <div class="card-body">
                        {{ App\ReservasiSewa::where('statuspengiriman', 'Dikirim')->count() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h4>Info Administrator</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Nama</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value=": {{ auth()->user()->name }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value=": {{ auth()->user()->email }}">
                        </div>
                    </div>
                   {{--  <div class="form-group row">
                        <label for="staticEmail" class="col-sm-4 col-form-label">Last Login</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value=": {{ auth()->user()->email }}">
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
