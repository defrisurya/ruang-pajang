@extends('layouts.dashcust')

@section('content')
<div class="section-content section-dasboard-home">
    <div class="container-fluid">
        <div class="dashboard-heading">
            <h2>My Account</h2>
            <p class="dashboard-subtitle">
                Informasi Akun Anda!
            </p>
        </div>
        <div class="dashboard-content">
            <div class="row mt-1">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label
                                                for="name-company"
                                                >Nama
                                            </label>
                                            <input
                                                type="text"
                                                class="form-control"
                                                name="name"
                                                id="name-company"
                                                value="{{ $data->name }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email"
                                                >Email
                                            </label>
                                            <input
                                                type="text"
                                                name="email"
                                                class="
                                                    form-control
                                                "
                                                id="email"
                                                value="{{ $data->email }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <div class="form-group">
                                            <label for="phone"
                                                >Phone
                                            </label>
                                            <input
                                                type="number"
                                                name="telpon"
                                                class="
                                                    form-control
                                                "
                                                id="phone"
                                                value="{{ $data->userdetail['telpon'] }}"
                                            />
                                        </div>
                                    </div>
    
                                    <div class="col-md-6 mt-2">
                                        <div class="form-group">
                                            <label for="alamat"
                                                >Alamat
                                            </label>
                                            <textarea
                                                name="alamat"
                                                id="alamat"
                                                class="
                                                    form-control
                                                "
                                            >{{ $data->userdetail['alamat'] }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <div class="form-group">
                                            <label for="provisi"
                                                >Provinsi
                                            </label>
                                            <input
                                                type="text"
                                                name="provinsi"
                                                class="
                                                    form-control
                                                "
                                                value="{{ $data->userdetail['provinsi'] }}"
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <div class="form-group">
                                            <label
                                                for="kabupaten"
                                                >Kabupaten
                                            </label>
                                            <input
                                                type="text"
                                                name="kabupaten"
                                                class="
                                                    form-control
                                                "
                                                id="phone"
                                                value="{{ $data->userdetail['kabupaten'] }}"
                                            />
                                        </div>
                                    </div>
    
                                    {{-- <div class="col-md-5 mt-2">
                                        <div class="form-group">
                                            <label
                                                for="kecamatan"
                                                >Kecamatan
                                            </label>
                                            <input
                                                type="text"
                                                class="
                                                    form-control
                                                "
                                                id="phone"
                                                value="{{ $data->userdetail['kecamatan'] }}"
                                            />
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-md-2 mt-2">
                                        <div class="form-group">
                                            <label for="kodepos"
                                                >Kode Pos
                                            </label>
                                            <input
                                                type="text"
                                                class="
                                                    form-control
                                                "
                                                id="kode-pos"
                                                value="1200"
                                            />
                                        </div>
                                    </div> --}}
    
                                    <div class="col-md-6 mt-2">
                                        <div class="form-group">
                                            <label
                                                for="username"
                                                >Password
                                            </label>
                                            <input
                                                type="password"
                                                name="password"
                                                class="
                                                    form-control
                                                "
                                                id="password"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col text-right">
                                        <button
                                            type="submit"
                                            class="
                                                btn btn-success
                                                mt-3
                                            "
                                            style="float: right"
                                        >
                                            Simpan
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection