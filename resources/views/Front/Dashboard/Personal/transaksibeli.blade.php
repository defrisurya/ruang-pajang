@extends('layouts.dashcust')

@section('content')
         <!--section content-->
         <div class="section-content section-dasboard-home">
            <div class="container-fluid">
                <div class="dashboard-heading">
                    <h2>Transaksi Pembelian</h2>
                    <p class="dashboard-subtitle">
                        Look what you have made today!
                    </p>
                </div>
                <div class="dashboard-content">
                    <div class="row mt-1">
                        <div class="col-md-12">
                            <div class="card">
                                <div
                                    class="
                                        card-body
                                        table-responsive
                                    "
                                >
                                    <table class="card-table table table-hover text-center">
                                        <thead>
                                            <tr>
                                                <th scope="col">
                                                    No
                                                </th>
                                                <th scope="col">
                                                    Invoice
                                                </th>
                                                <th scope="col">
                                                    Tanggal
                                                    Pembelian
                                                </th>

                                                <th scope="col">
                                                    Total Bayar
                                                </th>
                                                <th scope="col">
                                                    Status
                                                </th>
                                                <th scope="col">
                                                    Cek Detail
                                                </th>
                                            </tr>
                                        </thead>
                                        @php
                                            $no = 1;
                                        @endphp
                                        <tbody>
                                            @forelse ($data as $item)
                                                <tr>
                                                    <th scope="row">
                                                        {{ $no++ }}
                                                    </th>
                                                    <td>{{ $item->invoice }}</td>
                                                    <td>{{ Carbon\Carbon::parse($item->tanggalreservasi)->isoFormat('D MMMM Y') }}</td>

                                                    <td>Rp.{{ number_format($item->totalharga) }}</td>
                                                    <td>
                                                        <div class="status" style="color: red;">
                                                            {{ $item->statuspembayaran }}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p>
                                                            <a class="btn btn-primary" data-bs-toggle="collapse" href="#collapseExample{{ $item->id }}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                              Detail
                                                            </a>
                                                            <a target="_blank" class="btn btn-success" href="https://wa.me/6282138037647?text=Halo%20Admin%20Nomer%20Invoice%20Saya%20{{ $item->invoice }}">Wa</a>
                                                          </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6">
                                                        <div class="collapse" id="collapseExample{{$item->id}}">
                                                            <div class="card card-body">
                                                                <table class="table text-center">
                                                                    <thead>
                                                                      <tr>
                                                                        <th scope="col">Foto</th>
                                                                        <th scope="col">Dimensi</th>
                                                                        <th scope="col">Harga @</th>
                                                                        <th scope="col">Seniman</th>
                                                                      </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @forelse ($item->detailbeli  as $row)
                                                                            <tr>
                                                                                <td>
                                                                                    <img src="{{ asset($row->foto) }}" class="img-fluid" width="150px" alt="">
                                                                                </td>
                                                                                <td>{{ $row->dimensi }}</td>
                                                                                <td>Rp {{ number_format($row->harga) }}</td>
                                                                                <td>{{ $row->seniman }}</td>
                                                                            </tr>

                                                                        @empty

                                                                        @endforelse

                                                                    </tbody>
                                                                  </table>
                                                            </div>
                                                          </div>
                                                    </td>

                                                </tr>
                                            @empty
                                            <tr>
                                                <td colspan="6">Transaksi Beli Masih Kosong</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
