@extends('layouts.front')

@section('content')
            <div class="container mt-5">
                <div class="row justify-content-center">
                    <img src="{{ asset('front') }}/img/logo.png" style="width: 250px" alt="">
                </div>
                <div class="row justify-content-center mt-5">
                    <div class="col-md-10">
                        <p style="text-align: justify">
                            Pajangin adalah sebuah media yang dibangun untuk mendukung industry seni di Indonesia. Dibangun oleh Dini Art bekerjasama dengan Inovasi Nuswantara Digital, menyiapkan sebuah media bagi seniman dalam menampilkan karyanya baik secara online maupun offline. <br> <br>

                            Berpengalaman dalam kegiatan pameran offline di Jogja memacu kami untuk berkembang ke dunia digital dengan menggandeng para mitra dan menciptakan peluang bagi para seniman untuk unjuk karya baik secara online maupun offline. <br> <br>

                            Pajangin memiliki visi bahwa melalui seni, bersama menciptakan jalur untuk berbagi perspektif, membangun dan melibatkan komunitas, serta membentuk masa depan kolektif industry seni di Indonesia. <br> <br>

                            Pajangin beroperasi di Yogyakarta dan management office kami berada di Jl. Garuda No.18B Pringgolayan Banguntapan Bantul Yogyakarta
                            <br> <br> Contact info: 08123456789
                        </p>
                    </div>
                </div>
                <hr>

            @include('layouts.include.footerfront')

            </div>
@endsection


