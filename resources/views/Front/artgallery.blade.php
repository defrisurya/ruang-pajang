@extends('layouts.front')

@section('content')
        <div class="container">
            <section class="kategori mt-3">
                <h6>All Categories</h6>
                <div class="row mt-4 justify-content-center">
                    <div class="col-md-12 slide-slick text-center">
                        @forelse ($kategoris as $item)
                            <a style="text-decoration: none; color: black" href="{{ route('filter.kategori', $item->id) }}">
                                <div class="img-kategori">
                                    <img src="{{ asset($item->foto) }}" class="crop-kategori" alt="">
                                    <p>{{ $item->namakategori }}</p>
                                </div>
                            </a>
                        @empty
                            
                        @endforelse
                    </div>
                </div>
            </section>
            <section class="store-gallery mt-5">
                <h6 class="mb-4">Store Gallery</h6>
                <div class="row justify-content-center">

                    @forelse ($produks as $item)
                        <div class="col-md-4 mb-5 col-12">
                                <a style="text-decoration: none" href="{{ route('art.detail', $item->id) }}">
                                <div class="card border-0">
                                        <img src="{{ asset($item->foto) }}" class="crop-img-produk" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title text-center">{{ $item->pelukis['nama'] }}</h5>
                                        <p class="card-text text-center">
                                            {{ $item->judul }}, {{ $item->tahunpembuatan }} <br>
                                            {{ $item->media }} <br>
                                            {{ $item->dimensi }}
                                        </p>
                                    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                                    </div>
                                </div>
                            </a>
                        </div>
                    @empty
                        
                    @endforelse

                </div>
            </section>
            <hr>
            @include('layouts.include.footerfront')
        </div>
@endsection

@push('js')
<script>
    $('.slide-slick').slick({
        dots: false,
        infinite: true,
        speed: 100,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        responsive: [
            {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false,
                autoplay: true,
            }
            },
            {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                autoplay: true,
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: true,
            }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
        });
</script>
@endpush