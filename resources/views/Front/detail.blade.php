@extends('layouts.front')

@section('content')
    <div class="container">
        <section class="kategori mt-3">
            <h6>{{ $produk->pelukis['nama'] }}</h6>
            <div class="row g-0 mb-5">
                <div class="col-md-5 text-center">
                    <img src="{{ asset($produk->foto) }}" class="crop-img-detail-produk" alt="">
                </div>
                <div class="col-md-7">
                    <ul class="list-group" style="border: white;">
                        <li class="list-group-item" style="border: white;"><h6>{{ $produk->judul }}</h6></li>
                        <li class="list-group-item" style="border: white;">Media : {{ $produk->media }}</li>
                        <li class="list-group-item" style="border: white;">Dimension : {{ $produk->dimensi }}</li>
                        @if ($produk->status == 'tersewa')
                        @php
                            $latitude = $produk->userdetail['latitude'];

                            $langitude = $produk->userdetail['longitude'];
                        @endphp
                            <li class="list-group-item" style="border: white;">
                                Display Location : {{ $produk->userdetail->user['name'] }} <br>
                                <a style="text-decoration: none; color: black;" target="__blank" href="https://www.google.com/maps/?q={{ $latitude }},{{ $langitude }}"><img width="20px" src="https://img.icons8.com/ios-filled/50/000000/google-maps-new.png"/>MAP DIRECTION</a>
                            </li>
                        @endif
                        @if ($produk->status == 'tersedia')
                            <li class="list-group-item" style="border: white;">
                                Status : Tersedia <br>
                            </li>
                        @endif
                        <li class="list-group-item" style="border: white;">
                            Price Rent Rp {{ number_format($produk->hargasewa) }}/month <br>
                            Price Shop Rp {{ number_format($produk->hargajual) }}</li>
                        <li class="list-group-item" style="border: white;">
                            Deskripsi :
                            {{-- <p style="text-align: justify;"> --}}
                                {!! $produk->deskripsi !!}
                            {{-- </p> --}}
                        </li>
                        <form method="POST" action="{{ route('keranjangbeli') }}">
                            @csrf
                            <div class="mb-3 row">
                                <input type="hidden" value="{{ $produk->id }}" name="produkid">
                                <label for="inputPassword" class="col-sm-3 col-form-label"><b>Tawar Harga</b></label>
                                <div class="col-sm-6">
                                <input type="number" name="tawar" class="form-control" placeholder="Inputkan nominal penawaran">
                                </div>
                            </div>
                            @if ($produk->status == 'terjual')
                                <h5>Produk Sudah Terjual</h5>
                            @endif
                            @if ($produk->status != 'terjual')
                                <button class="btn btn-secondary col-md-7 col-12">Beli Karya</button>
                            @endif
                        </form>
                        @if ($produk->status == 'tersedia')
                            @if (auth()->user())
                               @if (auth()->user()->role == 'customercompany')
                                    <a class="btn btn-outline-secondary col-md-7 mt-3" href="{{ route('keranjangsewa',$produk->id) }}">Sewa Karya</a>
                               @endif
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </section>
        <hr>
        @include('layouts.include.footerfront')
    </div>
@endsection

