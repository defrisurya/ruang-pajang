@extends('layouts.front')

@section('content')
        <div class="container">
            <section class="kategori mt-3">
                <h6>{{ $pelukis->nama }}</h6>
                <div class="row">
                    <div class="row g-0 justify-content-center mt-4">
                        <button class="btn btn-outline-secondary col-md-3" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            ART WORK
                        </button>
                        <button class="btn btn-outline-secondary col-md-3 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            BIOGRAPHY
                        </button>
                        <button class="btn btn-outline-secondary col-md-3 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            CV-PROFILE
                        </button>
                    </div>

                    <div class="accordion mt-4" id="accordionExample">
                        <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <!-- <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Accordion Item #1
                            </button> -->
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <div class="row justify-content-center">
                                    <div class="col-md-12 mb-3 text-center">
                                        <h6>ART WORK</h6>
                                    </div>

                                    @forelse ($pelukis->produk as $item)
                                        <div class="col-md-4 mb-5 col-12">
                                            <a style="text-decoration:none" href="{{ route('art.detail', $item->id) }}">
                                                <div class="card">
                                                        <img src="{{ asset($item->foto) }}" class="card-img-top" alt="...">
                                                    <div class="card-body">
                                                        <h5 class="card-title text-center">{{ $pelukis->nama }}</h5>
                                                        <p class="card-text text-center">
                                                            {{  $item->judul  }}, {{ $item->tahunpembuatan }} <br>
                                                            {{ $item->media }} <br>
                                                            {{ $item->dimensi }}
                                                        </p>
                                                    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @empty
                                        
                                    @endforelse
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <!-- <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Accordion Item #2
                            </button> -->
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <div class="row">
                                    <div class="col-md-12 mb-3 text-center">
                                        <h6>BIOGRAPHY</h6>
                                    </div>
                                    <div class="col-md-6">
                                        {!! $pelukis->deskripsi !!}
                                    </div>
                                    <div class="col-md-6">
                                        <img src="{{ asset($pelukis->foto) }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <!-- <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Accordion Item #3
                            </button> -->
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <div class="row">
                                    <div class="col-md-12 mb-3 text-center">
                                        <h6>CV-PROFILE</h6>
                                    </div>
                                    <div class="col-md-6">
                                       {!! $pelukis->deskripsipengalaman !!}
                                    </div>
                                    <div class="col-md-6">
                                        <img src="img/artist.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                </div>
            </section>
            <hr>
            @include('layouts.include.footerfront')
        </div>
@endsection