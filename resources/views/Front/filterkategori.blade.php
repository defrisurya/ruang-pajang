@extends('layouts.front')

@section('content')
        <div class="container">
            <section class="store-gallery mt-5">
                <h6 class="mb-4">Filter By Kategori <span style="color: red"><b>{{ $kategori->namakategori }}</b></span></h6>
                <div class="row justify-content-center">

                    @forelse ($data as $item)
                        <div class="col-md-4 mb-5 col-12">
                                <a style="text-decoration: none" href="{{ route('art.detail', $item->id) }}">
                                <div class="card">
                                        <img src="{{ asset($item->foto) }}" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title text-center">{{ $item->pelukis['nama'] }}</h5>
                                        <p class="card-text text-center">
                                            {{ $item->judul }}, {{ $item->tahunpembuatan }} <br>
                                            {{ $item->media }} <br>
                                            {{ $item->dimensi }}
                                        </p>
                                    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                                    </div>
                                </div>
                            </a>
                        </div>
                    @empty
                        <div class="col-md-12 text-center">
                            <h2>Data Kategori Terpilih Masih Kosong</h2>
                        </div>
                    @endforelse

                </div>
            </section>
            <hr>
            @include('layouts.include.footerfront')
        </div>
@endsection

@push('js')
<script>
    $('.slide-slick').slick({
        dots: false,
        infinite: true,
        speed: 100,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        responsive: [
            {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false,
                autoplay: true,
            }
            },
            {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                autoplay: true,
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: true,
            }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
        });
</script>
@endpush