@extends('layouts.front')

@section('content')
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="{{ asset('front') }}/img/fotohome.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('front') }}/img/fotohome.png" class="d-block w-100" alt="...">
                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('front') }}/img/fotohome.png" class="d-block w-100" alt="...">
                                </div>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
                <section class="kategori mt-3">
                    <h5>Tren Categories</h5>
                    <div class="row mt-4 justify-content-center">
                        <div class="slide-slick">
                            @forelse ($kategoris as $kategori)
                            <div class="col-md-3 text-center">
                                <a style="text-decoration: none; color: black" href="{{ route('filter.kategori', $kategori->id) }}">
                                    <div class="img-kategori">
                                        <img src="{{ asset($kategori->foto) }}" class="crop-kategori" alt="">
                                        <p>{{ $kategori->namakategori }}</p>
                                    </div>
                                </a>
                            </div>
                            @empty

                            @endforelse
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12 text-center">
                            <p>
                                Enjoy  Buy and Rent Gallery <br>
                                From Best Artists
                            </p>
                        </div>
                        <div class="col-md-12 text-center">
                            <a class="btn btn-primary" href="{{ route('art.gallery') }}">View All Gallery</a>
                        </div>
                    </div>

                    <div class="row mt-4 mb-5 justify-content-center">
                        <div class="col-md-4 text-center">
                            <div class="card">
                                <div class="card-body">
                                    Our Colection <br>
                                    {{ $collection }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="card">
                                <div class="card-body">
                                    Our Artists <br>
                                    {{ $artist }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="card">
                                <div class="card-body">
                                    Our Client <br>
                                    {{ $client }}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <hr>

            @include('layouts.include.footerfront')

            </div>
@endsection

@push('js')
<script>
    $('.slide-slick').slick({
        dots: false,
        infinite: true,
        speed: 100,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        responsive: [
            {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false,
                autoplay: true,
            }
            },
            {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                autoplay: true,
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: true,
            }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
        });
</script>
@endpush
