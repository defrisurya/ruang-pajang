@extends('layouts.front')

@section('content')
<div class="container">
    <section class="kategori mt-3">
        <h6>Keranjang Beli</h6>
            <form action="{{ route('proses.orderbeli') }}" method="POST">
            @csrf
                    <div class="row mt-3">
                        @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                        @endif
                        <div class="col-md-8">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">KOLEKSI KARYA</th>
                                        <th scope="col">HARGA</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $no = 1;
                                        // dd($carts);
                                    @endphp
                                    @forelse ($carts as $key => $cart)
                                        <tr>
                                            <th scope="row">{{ $no++ }}</th>
                                            <td>
                                                <div class="row 9-0">
                                                    <div class="col-md-6">
                                                        <img class="img-fluid" src="{{ asset($cart['foto']) }}" alt="">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p class="chart-des-info">
                                                            <b>{{ $cart['pelukis'] }}</b> <br>
                                                            {{ $cart['judul'] }} <br>
                                                            {{ $cart['media'] }} <br>
                                                            {{ $cart['dimensi'] }} cm
                                                        </p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="chart-info-harga">Rp {{ number_format($cart['price']) }}</p>
                                                <a style="text-decoration: none;" href="{{ route('hapus.cart', $key) }}"><img src="https://img.icons8.com/bubbles/50/000000/delete-forever.png" alt=""></a>
                                            </td>
                                        </tr>
                                    @empty
                                        
                                    @endforelse
                                   
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                        <div class="col-md-4">
                            <div class="title-jangka-waktu">
                                ALAMAT KIRIM
                            </div>
                            <div class="row">
                                <div class="col-md-12 mt-3 mb-3 text-center">
                                    <div class="mb-3">
                                        <label class="form-label">Data Pembeli</label>
                                        <input type="text" name="namapembeli" class="form-control" readonly value="{{ auth()->user()->name }}">
                                    </div>
                                    <div class="mb-3">
                                        {{-- <label class="form-label">Handphone</label> --}}
                                        <input type="number" name="nohp" class="form-control" readonly value="{{ $userdetail->telpon }}">
                                    </div>
                                    <div class="mb-3">
                                        {{-- <label class="form-label">Pilih Provinsi</label> --}}
                                        <select class="form-select" aria-label="Default select example" name="province_id" id="province_id">
                                            <option value="">Provinsi Tujuan</option>
                                            @foreach ($provinsi  as $row)
                                            <option value="{{$row['province_id']}}" namaprovinsi="{{$row['province']}}">{{$row['province']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        {{-- <label class="form-label">Pilih Kota</label> --}}
                                        <select class="form-select" aria-label="Default select example" name="kota_id" id="kota_id">
                                            <option value="">Pilih Kabupaten</option>
                                        </select>
                                    </div>
                    
                                    {{-- <input class="form-control" type="text" value="{{ $berat }}" id="weight" name="weight"> --}}
                                    
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea1" class="form-label">Alamat Lengkap</label>
                                        <textarea name="alamatkirim" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    </div>
                                    {{-- @if ($userdetail->kabid == null)
                                        <p><b>Tambahkan Alamat Lengkap Anda Dulu</b></p>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            Tambah Alamat
                                        </button>
                                    @endif
                                    @if ($userdetail->kabid != null)
                                        <p>
                                            Dartno<br>
                                            Jln. Jodipati Sleman Sleman DiY  3987 <br> <br>
                                            <a style="text-decoration: none;" href="#">Edit Alamat</a>
                                        </p>
                                    @endif --}}
                                </div>
                                {{-- <div class="col-md-4 text-center mb-3">
                                    <button class="btn btn-outline-dark disabled mt-2">Utama</button>
                                </div> --}}
                            </div>
                            <div class="title-biaya-pemasangan mb-3">
                                PILIH EKSPEDISI
                            </div>
                            <div class="row g-0 justify-content-center mb-3">
                                <img class="col-md-3 col-3" src="{{ asset('front') }}/img/tiki.svg" alt="">
                                <img class="col-md-3 col-3" src="{{ asset('front') }}/img/jne.svg" alt="">
                            </div>
                            <div class="row mb-4">
                                    {{-- <input class="form-control" type="text" value="39" id="weight" name="city_origin"> --}}
                                    <div class="form-group mb-3">
                                        <label>Pilih Ekspedisi<span>*</span>
                                        </label>
                                        <select name="kurir" id="kurir" class="form-control">
                                        <option value="">Pilih kurir</option>
                                        <option value="jne">JNE</option>
                                        <option value="tiki">TIKI</option>
                                        <option value="pos">POS INDONESIA</option>
                                        </select>
                                    </div>
                    
                                    <div class="form-group">
                                        <label>Pilih Layanan<span>*</span>
                                        </label>
                                        <select name="layanan" id="layanan" class="form-control">
                                        <option value="">Pilih layanan</option>
                                        </select>
                                    </div>
                                <div class="col-md-6">
                                    <input type="hidden" id="hargatotal" name="totalharga">
                                    <input type="hidden" id="ongkoskirim" name="ongkoskirim">
                                    <input type="hidden" id="nama_provinsi" name="provinsi">
                                    <input type="hidden" id="nama_kota" name="namakota">
                                    <input type="hidden" value="{{ $berat }}" name="totalberat">
                                    <input type="hidden" value="{{ $jml }}" name="totalqty">
                                </div>
                            </div>
                        
                            <div class="deskripsi-info-harga card">
                                <div class="deskripsi-info-title-harga" style="padding: 5px;">
                                    <div class="col-md-12 text-center mb-2">RINCIAN ORDER</div>
                                    <div class="row">
                                        <table class="table">
                                            <tbody>
                                              <tr>
                                                <td>Total Harga Barang</td>
                                                <td>Rp {{ number_format($totalprice) }}</td>
                                              </tr>
                                              <tr>
                                                <td>Ongkos Kirim </td>
                                                <td id="ongkir"></td>
                                              </tr>
                                              <tr>
                                                <td><b>TOTAL PEMBAYARAN</b></td>
                                                <td id="totalharga"><b></b></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                    </div>
                                    <div class="row justify-content-center">
                                        <button class="btn btn-danger mt-3 col-md-8">Proses Order</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3 justify-content-center text-center mb-5">
                                <div class="col-md-12 text-center"><b>KONFIRMASI PEMBAYARAN</b></div>
                                <p class="mt-2">Harap Konfiramasi order dan lakukan pembayaran dalam jangka waktu 1 * 24 jam ke nomor rekening yang di berikan oleh Customer Service kami.</p>
                                <a class="btn btn-success col-md-8 mt-2" href="">WHATSAPP</a>
                            </div>
                        </div>
                    </div>
                </form>
    </section>
    <hr>
    @include('layouts.include.footerfront')
</div>
  
  <!-- Modal -->
  {{-- <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="">
                <div class="mb-3">
                    <label class="form-label">No Handphone</label>
                    <input type="number" class="form-control" value="{{ $userdetail->telpon }}">
                </div>
                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="province_id" id="province_id">
                        <option selected>Pilih Provinsi</option>
                        <option value="">Provinsi Tujuan</option>
                        @foreach ($provinsi  as $row)
                        <option value="{{$row['province_id']}}" namaprovinsi="{{$row['province']}}">{{$row['province']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="kota_id" id="kota_id" >
                    </select>
                </div>

                <input class="form-control" type="text" value="200" id="weight" name="weight">
                <input class="form-control" type="text" value="39" id="weight" name="city_origin">
                <div class="form-group mb-3">
                    <label>Pilih Ekspedisi<span>*</span>
                    </label>
                    <select name="kurir" id="kurir" class="form-control">
                    <option value="">Pilih kurir</option>
                    <option value="jne">JNE</option>
                    <option value="tiki">TIKI</option>
                    <option value="pos">POS INDONESIA</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Pilih Layanan<span>*</span>
                    </label>
                    <select name="layanan" id="layanan" class="form-control">
                    <option value="">Pilih layanan</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label">Alamat Lengkap</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div> --}}
@endsection

@push('js')
        <script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

        {{-- get kota --}}
        <script>
        $(document).ready(function(){
        //ini ketika provinsi tujuan di klik maka akan eksekusi perintah yg kita mau
        //name select nama nya "provinve_id" kalian bisa sesuaikan dengan form select kalian
        $('select[name="province_id"]').on('change', function(){
            // membuat variable namaprovinsiku untyk mendapatkan atribut nama provinsi
            var namaprovinsiku = $("#province_id option:selected").attr("namaprovinsi");
            // menampilkan hasil nama provinsi ke input id nama_provinsi
            $("#nama_provinsi").val(namaprovinsiku);

            // kita buat variable provincedid untk menampung data id select province
            let provinceid = $(this).val();
            //kita cek jika id di dpatkan maka apa yg akan kita eksekusi
        if(provinceid){
        // jika di temukan id nya kita buat eksekusi ajax GET
            jQuery.ajax({
                // url yg di root yang kita buat tadi
                url:"/kota/"+provinceid,
                // aksion GET, karena kita mau mengambil data
                type:'GET',
                // type data json
                dataType:'json',
                // jika data berhasil di dapat maka kita mau apain nih
                success:function(data){
                    // jika tidak ada select dr provinsi maka select kota kososng / empty
                    // $('select[name="kota_id"]').empty();
                    // jika ada kita looping dengan each
                    $.each(data, function(key, value){
                        // perhtikan dimana kita akan menampilkan data select nya, di sini saya memberi name select kota adalah kota_id
                        $('select[name="kota_id"]').append('<option value="'+ value.city_id +'" namakota="'+ value.type +' ' +value.city_name+ '">' + value.type + ' ' + value.city_name + '</option>');
                    });
                }
            });
            }else {
        $('select[name="kota_id"]').empty();
        }
        });
            //memberikan action ketika select name kota_id di select
            $('select[name="kota_id"]').on('change', function(){
            // membuat variable namakotaku untyk mendapatkan atribut nama kota
            var namakotaku = $("#kota_id option:selected").attr("namakota");
            // menampilkan hasil nama provinsi ke input id nama_provinsi
            $("#nama_kota").val(namakotaku);
            });
        });
    </script>
    
    {{-- get ongkir --}}
    <script>
        var berat =  <?php echo json_encode($berat); ?>;
        $('select[name="kurir"]').on('change', function(){
            // kita buat variable untuk menampung data data dari  inputan
            // name city_origin di dapat dari input text name city_origin
            // let origin = $("input[name=city_origin]").val();
            // name kota_id di dapat dari select text name kota_id
            let destination = $("select[name=kota_id]").val();
            // name kurir di dapat dari select text name kurir
            let courier = $("select[name=kurir]").val();
            // name weight di dapat dari select text name weight
            // let weight = $("input[name=weight]").val();
            // alert(courier);
            if(courier){
                jQuery.ajax({
                    url:"/origin="+39+"&destination="+destination+"&weight="+berat+"&courier="+courier,
                    type:'GET',
                    dataType:'json',
                    success:function(data){
                    // $('select[name="layanan"]').empty();
                    // ini untuk looping data result nya
                    $.each(data, function(key, value){
                        // ini looping data layanan misal jne reg, jne oke, jne yes
                        $.each(value.costs, function(key1, value1){
                                // ini untuk looping cost nya masing masing
                                // silahkan pelajari cara menampilkan data json agar lebi paham
                                $.each(value1.cost, function(key2, value2){
                                $('select[name="layanan"]').append('<option value="'+ value2.value +'">' + value1.service + '-' + value1.description + '-' +value2.value+ '</option>');
                                });
                            });
                        });
                    }
                    });
                } else {
                $('select[name="layanan"]').empty();
            }
        });
    </script>

        {{-- get biaya ongkos --}}
    <script>
        var totalan =  <?php echo json_encode($totalprice); ?>;
         $('select[name="layanan"]').on('change', function(){

            const formatRupiah = (money) => {
            return new Intl.NumberFormat('id-ID',
                { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
            ).format(money);
            }
            // console.log(formatRupiah(15000));

            // ongkir
            var ongkos = $("#layanan option:selected").attr("value");
            document.getElementById('ongkir').innerHTML = formatRupiah(ongkos);
            $("#ongkoskirim").val(ongkos);

            // totalbayar
            var totalharga = parseInt(ongkos)+parseInt(totalan);
            console.log(totalharga);
            document.getElementById('totalharga').innerHTML = formatRupiah(totalharga);
            $("#hargatotal").val(totalharga);
         });
    </script>
@endpush