@extends('layouts.front')

@section('content')
<div class="container">
    <section class="kategori mt-3">
        <h6>Keranjang Sewa</h6>
        <form action="{{ route('proses.ordersewa') }}" method="POST">
            @csrf
            <div class="row mt-3">
                @if (session('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>
                @endif
                <div class="col-md-8">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">KOLEKSI KARYA</th>
                                <th scope="col">HARGA</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                    // dd($carts);
                                @endphp
                                @forelse ($cart as $key => $cart)
                                    <tr>
                                        <th scope="row">{{ $no++ }}</th>
                                        <td>
                                            <div class="row 9-0">
                                                <div class="col-md-6">
                                                    <img class="img-fluid" src="{{ asset($cart['foto']) }}" alt="">
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="chart-des-info">
                                                        <b>{{ $cart['pelukis'] }}</b> <br>
                                                        {{ $cart['judul'] }} <br>
                                                        {{ $cart['media'] }} <br>
                                                        {{ $cart['dimensi'] }}
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <p class="chart-info-harga">Rp {{ number_format($cart['price']) }}</p>
                                            <a style="text-decoration: none;" href="{{ route('hapussewa', $key) }}"><img src="https://img.icons8.com/bubbles/50/000000/delete-forever.png" alt=""></a>
                                        </td>
                                    </tr>
                                    @empty
                                @endforelse
                            </tbody>
                        </table>
                        <div>
                            <a href="{{ route('our.artist') }}" class="btn btn-warning"><img src="https://img.icons8.com/fluency-systems-filled/48/000000/plus.png" width="20"/> Tambah Produk</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="title-jangka-waktu">
                        JANGKA WAKTU SEWA
                    </div>
                            @php
                                $tanggal1 = date('Y-m-d');
                                $tanggal = date('Y-m-d', strtotime('+8 days', strtotime( $tanggal1 )));
                            @endphp
                        <div class="row">
                            <div class="col-md-12 mt-3 mb-3 text-center">
                                <div class="mb-3">
                                    <label class="form-label">Jangka waktu sewa minimal 1 bulan</label>
                                    <label class="form-label">TANGGAL MULAI</label>
                                    <input type="date" name="tglmulai" min="{{ $tanggal }}" value="{{ $tanggal }}" class="form-control" id="input1">
                                </div>
                                <div class="mb-3" align="center">
                                    <label class="form-label text-center-mb-2">LAMA SEWA</label>
                                    <select class="form-select text-center" aria-label="Default select example" name="lamabulan" id="lamabulan">
                                        <option value="">Lama Sewa</option>
                                        <option value="1">1 Bulan</option>
                                        <option value="2">2 Bulan</option>
                                        <option value="3">3 Bulan</option>
                                        <option value="4">4 Bulan</option>
                                        <option value="5">5 Bulan</option>
                                        <option value="6">6 Bulan</option>
                                        <option value="7">7 Bulan</option>
                                        <option value="8">8 Bulan</option>
                                        <option value="9">9 Bulan</option>
                                        <option value="10">10 Bulan</option>
                                        <option value="11">11 Bulan</option>
                                        <option value="12">12 Bulan / 1 Tahun<option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="exampleFormControlTextarea1" class="form-label">Alamat Lengkap</label>
                                    <textarea name="alamatkirim" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="title-biaya-pemasangan mb-3">
                                BIAYA PENGIRIMAN & PEMASANGAN
                            </div>
                            <div class="mb-3" align="center">
                                <label class="form-label text-center-mb-2">LOKASI WILAYAH/KABUPATEN</label>
                                <select class="form-select text-center" aria-label="Default select example" name="tarifsewa" id="tarifsewa">
                                    <option value="">Pilih Wilayah/Kabupaten</option>
                                    @foreach ($ongkir as $item)
                                    <option value="{{ $item->tarif }}" kota="{{ $item->kabupaten }}">{{ $item->kabupaten }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 text-center mb-2"> Ongkos Kirim <span id="ongkir"></span></div>
                            <div class="deskripsi-info-harga card">
                                <div class="deskripsi-info-title-harga" style="padding: 5px;">
                                    <div class="col-md-12 text-center mb-2"><h5>TOTAL HARGA</h5></div>
                                    <div class="col-md-12 text-center mb-2"><b><span id="total"></span></b></div>
                                    {{-- <div class="col-md-12 text-center mb-2">
                                        <span>Jangka waktu 1 bulan Mulai</span><br>
                                        <span id="result1"></span> - <span id="result2"></span>
                                    </div> --}}
                                    <div class="col-md-6">
                                        <input type="hidden" id="jmltotal" name="jmltotal">
                                        <input type="hidden" id="jmltotalsewabulan" name="jmltotalsewabulan">
                                        <input type="hidden" id="kota" name="kota">
                                        <input type="hidden" id="diskon" name="diskon">
                                        <input type="hidden" id="totalfix" name="totalfix">
                                    </div>
                                    <br>
                                    <div class="col-md-12 text-center mb-2"><h3>METODE PEMBAYARAN</h3></div>

                                    @php
                                        // dd(auth()->user()->userdetail['premium']);
                                    @endphp

                                    <div class="mb-3" align="center">
                                        <label class="form-label text-center-mb-2">GUNAKAN BARTER VOUCHER</label>
                                        <select {{ (auth()->user()->userdetail['premium']) == 'off' ? 'disabled' : '' }} {{ (auth()->user()->userdetail['premium']) == '' ? 'disabled' : '' }}  class="form-select text-center" aria-label="Default select example" name="vouchersewa" id="vouchersewa">
                                            <option value="">Pilih Barter Voucher</option>
                                            @foreach ($vc as $items)
                                                <option value="{{ $items->hargavoucher }}" persen="{{ $items->persen }}">{{ $items->namavoucher }} Senilai Rp {{ number_format($items->hargavoucher) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-md-12 text-center mb-2" id="vcsewa"></div> --}}
                                    {{-- <div class="col-md-12 text-center mb-2"><h5>TOTAL HARGA</h5></div>
                                    <div class="col-md-12 text-center mb-2" id="jmltotal" ></div> --}}
                                    <div class="col-md-12 text-center mb-2"><p>Barter hanya bisa Digunakan Oleh <br> Member Premium <br> <b><span id="persendiskon">0</span>%</b><br>dari total biaya sewa. Nilai Potongan<br><b><span style="color: orange" id="vcsewa"></span></b></p></div>
                                        <div class="col-md-12 text-center mb-1">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                Klik disini untuk informasi Barter
                                            </button>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="mb-3 form-check justify-content-center">
                                        <input type="checkbox" style="width: 17px; height: 20px;" oninput="validasi()" name="syarat" class="form-check-input" id="syarat">
                                        <label class="form-check-label" for="exampleCheck1"><a href="" class="text-decoration-none">Setujui Syarat dan Ketentuan sebelum proses order.</a> </label>
                                    </div>
                                    <div class="row justify-content-center">
                                        {{-- <input class="btn btn-warning mt-2 col-md-8" type="reset"> --}}
                                        <button name="agree" class="btn btn-danger mt-3 col-md-8" disabled="">Proses Order</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3 justify-content-center text-center mb-5">
                                <div class="col-md-12 text-center"><b>UPGRADE MEMBER PREMIUM</b></div>
                                <p class="mt-2">Harap Menghubungi Customer Service kami.</p>
                                <a class="btn btn-success col-md-8 mt-2" href="">WHATSAPP</a>
                            </div>
                </div>
            </div>
        </form>
    </section>
<hr>
@include('layouts.include.footerfront')
</div>

<!-- Modal Informasi Voucher -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Informasi Barter</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
@push('js')
{{-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script> --}}
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>

<script>
    input1.oninput = function() {
        result1.innerHTML = input1.value;
    };
</script>

{{-- lama bulan kali harga total --}}
<script type="text/javascript">
$(document).ready(function(e) {
    var totalbayar2 =  <?php echo json_encode($totalsewa); ?>;
    $('select[name="lamabulan"]').on('change', function(){
        var lamabulan = $(this).val();
        var totalbayar3 = totalbayar2 * lamabulan;
        // alert(totalbayar3);
        $('select[name="tarifsewa"]').val('');
        $('select[name="vouchersewa"]').val('');
        document.getElementById('total').innerHTML = '';
        document.getElementById('ongkir').innerHTML = '';
        document.getElementById('vcsewa').innerHTML = '';

        $("#jmltotalsewabulan").val(totalbayar3);
    });
});
</script>


<script type="text/javascript">
$(document).ready(function(e) {
// var totalbayar =  <?php echo json_encode($totalsewa); ?>;
    $('select[name="tarifsewa"]').on('change', function(){
        // alert('here');
        const formatRupiah = (money) => {
            return new Intl.NumberFormat('id-ID',
                { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
            ).format(money);
            }

            var kota = $('option:selected', this).attr('kota');
            // alert(option)
            $("#kota").val(kota);


        // Ongkir
        var ongkoskirim = $(this).val();
        // alert(ongkoskirim);
        document.getElementById('ongkir').innerHTML = formatRupiah(ongkoskirim);
        var totalbayar = document.getElementById("jmltotalsewabulan").value;

        // Total Bayar Sewa
        var total = parseInt(ongkoskirim)+parseInt(totalbayar);
        // console.log(total);
        // reset
        $('select[name="vouchersewa"]').val('');
        document.getElementById('vcsewa').innerHTML = '';

        document.getElementById('total').innerHTML = formatRupiah(total);
        $("#jmltotal").val(total);
        $("#totalfix").val(total);

    });
});
</script>


<script type="text/javascript">
$(document).ready(function(e) {
    $('select[name="vouchersewa"]').on('change', function(){
        // alert('here');
        const formatRupiah = (money) => {
            return new Intl.NumberFormat('id-ID',
                { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
            ).format(money);
            }

        var vcsewa = $(this).val();

        // get attribute option
        var persen = this.querySelector(':checked').getAttribute('persen');
        document.getElementById('persendiskon').innerHTML = persen;
        // alert(persen / 100);
        var potongan = persen/100;
        // Total Bayar Menggunakan Voucher
        // var totalbayar = document.getElementById("jmltotal").value;
        var tarifsewa = document.getElementById("tarifsewa").value;
        var totalbayar = document.getElementById("jmltotalsewabulan").value;
        var voucher = totalbayar * potongan;
        // alert(tarifsewa);
        
        document.getElementById('vcsewa').innerHTML = formatRupiah(voucher);
        var jmltotal = parseInt(totalbayar)-parseInt(voucher)+parseInt(tarifsewa);
        console.log(jmltotal);
        document.getElementById('total').innerHTML = formatRupiah(jmltotal);
        $("#diskon").val(voucher);
        $("#totalfix").val(jmltotal);
    });
});
</script>




<script>
    var input = $( ":reset" ).css({
});
</script>

<script>
// Validasi Submit
    $("input[type=checkbox]").on( "change", function(evt) {
    var syarat = $('input[id=syarat]:checked');
    if(syarat.length == 0){
        $("button[name=agree]").prop("disabled", true);
    }else{
        $("button[name=agree]").prop("disabled", false);
    }
});
</script>
@endpush
