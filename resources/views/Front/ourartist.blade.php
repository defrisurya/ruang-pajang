@extends('layouts.front')

@section('content')
        <div class="container">
            <section class="kategori mt-3">
                <h6>Our Artist</h6>
                @forelse ($pelukis as $row)
                    <div class="row mt-4 justify-content-center mb-5">
                        <div class="col-md-3 col-12 mb-2">
                            <h4>{{ $row->nama }}</h4> <br>
                            <img class="img-fluid" src="https://img.icons8.com/fluency/48/000000/easel.png"/>&nbsp; {{ $row->aliranlukis }} <br> <br>
                            <a class="btn btn-outline-primary" href="{{ route('artis.detail', $row->id) }}"> <img width="20px" src="https://img.icons8.com/color/48/000000/double-right--v2.png"/> View Gallery</a>
                        </div>
                        <div class="col-md-9 col-12 slide-slick">
                            @forelse ($row->produk as $item)
                                @if ($item->status == 'tersedia')
                                    <div class="img-kategori">
                                        <a href="{{ route('art.detail', $item->id) }}">
                                            <img class="img-artist" src="{{ asset($item->foto) }}" alt="">
                                        </a>
                                    </div>
                                @endif
                            @empty
                                
                            @endforelse
                        </div>
                    </div>
                @empty
                    
                @endforelse
            </section>
        
            <hr>
            @include('layouts.include.footerfront')
        </div>
@endsection

@push('js')
    <script>
        $('.slide-slick').slick({
            dots: false,
            infinite: true,
            speed: 100,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            arrows: false,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false,
                    autoplay: true,
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    autoplay: true,
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    autoplay: true,
                }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
            });
    </script>
@endpush