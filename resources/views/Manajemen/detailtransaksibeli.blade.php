@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <h4>Detail Data Transaksi Beli {{ $data->user['name'] }}</h4>
            </div>
            <div class="card-body">
                <table class="card-table table table-hover text-center">
                    <thead>
                        <tr>
                            <th scope="col">Foto</th>
                            <th scope="col">Dimensi</th>
                            <th scope="col">Harga @</th>
                            <th scope="col">Pendapatan Pelukis 60%</th>
                            <th scope="col">Seniman</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data->detailbeli  as $row)
                        <tr>
                            <td>
                                <img src="{{ asset($row->foto) }}" class="img-fluid mt-2" width="150px" alt="">
                            </td>
                            <td>{{ $row->dimensi }}</td>
                            <td>{{ number_format($row->harga) }}</td>
                            <td><span class="badge rounded-pill bg-info text-dark">{{ number_format($row->harga * 0.6) }}</span></td>
                            <td><span class="badge rounded-pill bg-dark">{{ $row->seniman }}</span></td>
                        </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>
                <tr>
                    <td>
                        <p style="float: right">
                            <b>Total Harga</b> Rp {{ number_format($data->totalharga - $data->ongkoskirim) }}
                        </p>
                    </td>
                    <td>
                        <a class="btn btn-primary btn-lg" href="{{ route('m.transaksi.beli') }}">
                            Kembali
                        </a>
                    </td>
                </tr>
            </div>
        </div>
    </div>
</div>
@endsection
