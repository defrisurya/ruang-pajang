@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <h4>Data Sharing Transaksi Sewa</h4>
            </div>
            <div class="card-body">
                <table class="card-table table table-hover text-center">
                    <thead>
                        <tr>
                            <th scope="col">
                                No
                            </th>
                            <th scope="col">
                                Invoice
                            </th>
                            <th scope="col">
                                Customer
                            </th>
                            <th scope="col">
                                Total
                            </th>
                            {{-- <th scope="col">
                                Pelukis 40%
                            </th> --}}
                            <th scope="col">
                                PT INDI 60%
                            </th>
                            <th scope="col">
                                Aksi
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @forelse ($data as $item)
                            <tr>
                                <th scope="row">{{ $no++ }}</th>
                                <td>{{ $item->invoice }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ number_format($item->totalharga - $item->ongkoskirim) }}</td>
                                {{-- <td>
                                    <span class="badge rounded-pill bg-success">{{ number_format(round(($item->totalharga - $item->ongkoskirim) * 0.4)) }}</span>
                                </td> --}}
                                <td>
                                    <span class="badge rounded-pill bg-info text-dark">{{ number_format(round(($item->totalharga - $item->ongkoskirim) * 0.6)) }}</span>
                                </td>
                                <td>
                                    <p>
                                        {{-- <a class="btn btn-success" href="#">
                                            Konfirmasi
                                        </a> --}}
                                        <a class="btn btn-primary" href="{{ route('detail.m.sewa', $item->id) }}">
                                            Detail
                                        </a>
                                    </p>
                                </td>
                            </tr>
                            
                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

        
    </div>
</div>
@endsection
