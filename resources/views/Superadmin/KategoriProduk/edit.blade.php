@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-10 ml-5">
              <div class="row">
                <div class="col-md-12 text-center">
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{ $error }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    @endforeach
                </div>
              </div>
          <div class="card">
            <div class="card-header">
                <h4>Ubah Data {{ $kategoriproduk->namakategori }}</h4>
            </div>
            <div class="card-body">
             <form action="{{ route('kategoriproduk.update', $kategoriproduk) }}" method="POST" enctype="multipart/form-data">
                 @csrf
                 @method('PUT')
                 <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Nama Kategori</label>
                    <input type="text" class="form-control" name="namakategori" placeholder="nama kategori" value="{{ old('namakategori', $kategoriproduk->namakategori) }}">
                  </div>
                  <div class="mb-3">
                    <label for="formFile" class="form-label">Foto</label>
                      <input class="form-control" type="file" name="foto" id="formFile">
                    <div id="emailHelp" class="form-text text-info">Biarkan Kosong Jika Tidak Mengganti File</div>
                      <div class="form-group mt-2" style="max-width: 20rem;">
                        <img width="300" src="{{ asset($kategoriproduk->foto) }}" alt="">
                      </div>
                  </div>
                 <div class="col-md-12 mb-5 text-center">
                     <button class="btn btn-primary">Simpan</button>
                 </div>
             </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
