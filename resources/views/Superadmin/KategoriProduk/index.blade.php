@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-7 card">
            <div class="card-header">
                <h4>Semua Data Kategori Produk</h4>
            </div>
            <div class="card-body">
                <table class="table text-center table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Kategori</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $item => $kategoriproduk)
                            <tr>
                                <th scope="row">{{ $data->firstItem() + $item }}</th>
                                <td>{{ $kategoriproduk->namakategori }}</td>
                                <td><img src="{{ asset($kategoriproduk->foto) }}" class="img-fluid mb-1" style="max-width: 150px" alt=""></td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <b><i class="fas fa-ellipsis-v"></i></b>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a href="{{ route('kategoriproduk.edit', $kategoriproduk) }}" class="dropdown-item">Edit</a>
                                            <form action="{{ route('kategoriproduk.destroy', $kategoriproduk) }}" method="POST" onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                                {!! method_field('delete') . csrf_field() !!}
                                                <button class="dropdown-item" type="submit">
                                                    Delete
                                                </button>
                                            </form>
                                        </div>
                                </td>
                            </tr>
                        @empty
                            
                        @endforelse
                     
                    </tbody>
                  </table>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12 text-center">
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{ $error }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    @endforeach
                </div>
            </div>
          <div class="card">
            <div class="card-header">
                <h4>Tambah Data Kategori</h4>
            </div>
            <div class="card-body">
             <form action="{{ route('kategoriproduk.store') }}" method="POST" enctype="multipart/form-data">
                 @csrf
                 <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Nama Kategori</label>
                    <input type="text" class="form-control" name="namakategori" placeholder="nama kategori">
                  </div>
                 <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Foto</label>
                    <input type="file" class="form-control" name="foto">
                  </div>
                 <div class="col-md-12 mb-5 text-center">
                     <button class="btn btn-primary">Simpan</button>
                 </div>
             </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
