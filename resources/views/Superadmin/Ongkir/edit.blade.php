@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-10 ml-5">
              <div class="row">
                <div class="col-md-12 text-center">
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{ $error }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    @endforeach
                </div>
              </div>
          <div class="card">
            <div class="card-header">
                <h4>Ubah Data {{ $ongkir->kabupaten }}</h4>
            </div>
            <div class="card-body">
             <form action="{{ route('ongkir.update', $ongkir) }}" method="POST">
                 @csrf
                 @method('PUT')
                 <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Nama Kabupaten</label>
                    <input type="text" class="form-control" name="kabupaten" placeholder="Nama Kabupaten" value="{{ old('kabupaten', $ongkir->kabupaten) }}">
                  </div>
                
                  <label for="exampleFormControlInput1" class="form-label">Tarif</label>
                  <div class="input-group mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-default">Rp</span>
                    <input type="number" name="tarif" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="tarif" value="{{ old('tarif', $ongkir->tarif) }}">
                  </div>
                  
                 <div class="col-md-12 mb-5 text-center">
                     <button class="btn btn-primary">Simpan</button>
                 </div>
             </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
