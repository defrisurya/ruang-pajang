@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-7 card">
            <div class="card-header">
                <h4>Semua Data Ongkir</h4>
            </div>
            <div class="card-body">
                <table class="table text-center table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Kabupaten</th>
                        <th scope="col">Tarif</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $item => $ongkir)
                            <tr>
                                <th scope="row">{{ $data->firstItem() + $item }}</th>
                                <td>{{ $ongkir->kabupaten }}</td>
                                <td>
                                    Rp {{ number_format($ongkir->tarif) }}
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <b><i class="fas fa-ellipsis-v"></i></b>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a href="{{ route('ongkir.edit', $ongkir) }}" class="dropdown-item">Edit</a>
                                            <form action="{{ route('ongkir.destroy', $ongkir) }}" method="POST" onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                                {!! method_field('delete') . csrf_field() !!}
                                                <button class="dropdown-item" type="submit">
                                                    Delete
                                                </button>
                                            </form>
                                        </div>
                                </td>
                            </tr>
                        @empty
                            
                        @endforelse
                     
                    </tbody>
                  </table>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12 text-center">
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{ $error }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    @endforeach
                </div>
            </div>
          <div class="card">
            <div class="card-header">
                <h4>Tambah Data Ongkir</h4>
            </div>
            <div class="card-body">
             <form action="{{ route('ongkir.store') }}" method="POST">
                 @csrf
                 <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Nama Kabupaten</label>
                    <input type="text" class="form-control" name="kabupaten" placeholder="nama kabupaten" value="{{ old('kabupaten') }}">
                  </div>

                  <label for="exampleFormControlInput1" class="form-label">Tarif</label>
                  <div class="input-group mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-default">Rp</span>
                    <input type="number" name="tarif" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="tarif" value="{{ old('tarif') }}">
                  </div>

                 <div class="col-md-12 mb-5 text-center">
                     <button class="btn btn-primary">Simpan</button>
                 </div>
             </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
