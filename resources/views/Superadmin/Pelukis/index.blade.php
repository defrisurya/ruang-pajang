@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <div class="col-md-6"><h4>Semua Data Pelukis</h4></div>
                <div class="col-md-6 text-right">
                    <a class="btn btn-primary" href="{{ route('pelukis.create') }}">Add</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table text-center table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Pelukis</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Aliran Lukis</th>
                        <th scope="col">Tahun Karya</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $item => $pelukis)
                            <tr>
                                <th scope="row">{{ $data->firstItem() + $item }}</th>
                                <td>{{ $pelukis->nama }}</td>
                                <td><img src="{{ asset($pelukis->foto) }}" class="img-fluid mb-1" style="max-width: 150px" alt=""></td>
                                <td>{{ $pelukis->aliranlukis }}</td>
                                <td>{{ $pelukis->tahunkarya }}</td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-info btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <b><i class="fas fa-ellipsis-v"></i></b>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a href="{{ route('pelukis.edit', $pelukis) }}" class="dropdown-item">Edit</a>
                                            <form action="{{ route('pelukis.destroy', $pelukis) }}" method="POST" onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                                {!! method_field('delete') . csrf_field() !!}
                                                <button class="dropdown-item" type="submit">
                                                    Delete
                                                </button>
                                            </form>
                                        </div>
                                </td>
                            </tr>
                        @empty
                            
                        @endforelse
                     
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
@endsection
