@extends('layouts.stisla')
@section('css')
    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
@endsection

@section('content')
<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-10">
              <div class="row">
                <div class="col-md-12 text-center">
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{ $error }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    @endforeach
                </div>
              </div>
          <div class="card">
            <div class="card-header">
                <h4>Tambah Data Pelukis</h4>
            </div>
            <div class="card-body">
             <form action="{{ route('produk.store') }}" method="POST" enctype="multipart/form-data">
                 @csrf
                 <input type="hidden" name="status" value="tersedia">
                  <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Kategori</label>
                    <select class="form-select form-control" name="kategori_id" aria-label="Default select example">
                        <option value="">-- Pilih Salah Satu --</option>
                        @forelse ($kategori as $item)
                            <option value="{{ $item->id }}">{{ $item->namakategori }}</option>
                        @empty
                            
                        @endforelse
                      </select>
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Pelukis</label>
                    <select class="form-select form-control" name="pelukis_id" aria-label="Default select example">
                        <option value="">-- Pilih Salah Satu --</option>
                        @forelse ($pelukis as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @empty
                            
                        @endforelse
                      </select>
                  </div>
                <div class="mb-3">
                    <label class="form-label">Judul</label>
                    <input type="text" class="form-control" name="judul" placeholder="judul lukisan" value="{{ old('judul') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Aliran Lukis</label>
                    <input type="text" class="form-control" name="aliranlukis" placeholder="aliran lukis" value="{{ old('aliranlukis') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Tahun Pembuatan</label>
                    <input type="number" class="form-control" name="tahunpembuatan" placeholder="tahun karya" value="{{ old('tahunpembuatan') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Harga Sewa</label>
                    <input type="number" class="form-control" name="hargasewa" placeholder="harga sewa" value="{{ old('hargasewa') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Harga Jual</label>
                    <input type="number" class="form-control" name="hargajual" placeholder="harga jual" value="{{ old('hargajual') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Harga Fix Jual</label>
                    <input type="number" class="form-control" name="hargafix" placeholder="harga fix jual" value="{{ old('hargafix') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Dimensi</label>
                    <input type="text" class="form-control" name="dimensi" placeholder="dimensi" value="{{ old('dimensi') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Media</label>
                    <input type="text" class="form-control" name="media" placeholder="media" value="{{ old('media') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Berat</label>
                    <input type="number" class="form-control" name="berat" placeholder="berat" value="{{ old('berat') }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Deskripsi</label>
                    <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{ old('deskripsi') }}</textarea>
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Foto</label>
                    <input class="form-control" type="file" name="foto" id="formFile">
                </div>
                <div class="col-md-12 mb-5 text-center">
                    <button class="btn btn-primary">Simpan</button>
                </div>
             </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    CKEDITOR.replace( 'deskripsi' );
</script>
@endsection
