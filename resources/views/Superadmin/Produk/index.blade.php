@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <div class="col-md-6"><h4>Semua Data Kategori Produk</h4></div>
                <div class="col-md-6 text-right">
                    <a class="btn btn-primary" href="{{ route('produk.create') }}">Add</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table text-center table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        {{-- <th scope="col">Nama Kategori</th> --}}
                        <th scope="col">Nama Pelukis</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Harga Sewa/Bulan</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $item => $produk)
                            <tr>
                                <th scope="row">{{ $data->firstItem() + $item }}</th>
                                {{-- <td>{{ $produk->kategori['namakategori'] }}</td> --}}
                                <td>{{ $produk->pelukis['nama'] }}</td>
                                <td><img src="{{ asset($produk->foto) }}" class="img-fluid mb-1" style="max-width: 150px" alt=""></td>
                                <td>Rp {{ number_format($produk->hargasewa) }}</td>
                                <td>
                                    @if ($produk->status == 'tersedia')
                                        <span class="badge bg-primary text-white">{{ $produk->status }}</span>
                                    @endif
                                    @if ($produk->status == 'tersewa')
                                        <span class="badge bg-success text-white">{{ $produk->status }}</span>
                                    @endif
                                    @if ($produk->status == 'terjual')
                                        <span class="badge bg-danger text-white">{{ $produk->status }}</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <b><i class="fas fa-ellipsis-v"></i></b>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a href="{{ route('produk.edit', $produk) }}" class="dropdown-item">Edit</a>
                                            <form action="{{ route('produk.destroy', $produk) }}" method="POST" onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                                {!! method_field('delete') . csrf_field() !!}
                                                <button class="dropdown-item" type="submit">
                                                    Delete
                                                </button>
                                            </form>
                                        </div>
                                </td>
                            </tr>
                        @empty
                            
                        @endforelse
                     
                    </tbody>
                  </table>
                  {{ $data->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
