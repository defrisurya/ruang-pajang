@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <h4>Detail Data Transaksi Sewa</h4>
            </div>
            <div class="card-body">
                <table class="card-table table table-hover text-center">
                    <thead>
                        <tr>
                            <th scope="col">Foto</th>
                            <th scope="col">Dimensi</th>
                            <th scope="col">Harga @</th>
                            <th scope="col">Seniman</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data->detailsewa  as $row)
                        <tr>
                            <td>
                                <img src="{{ asset($row->foto) }}" class="img-fluid" width="150px" alt="">
                            </td>
                            <td>{{ $row->dimensi }}</td>
                            <td>Rp {{ number_format($row->harga) }}</td>
                            <td>{{ $row->seniman }}</td>
                        </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>
                <tr>
                    <td>
                        <p style="float: right">
                            <a class="btn btn-success" href="{{ route('konfirmasi.sewa', $data->id) }}">
                                Konfirmasi
                            </a>
                            <a class="btn btn-danger" href="{{ route('tolaksewa', $data->id) }}">
                                Tolak
                            </a>
                            <a class="btn btn-primary" href="{{ route('t.sewa.s') }}">
                                Kembali
                            </a>
                        </p>
                    </td>
                </tr>
            </div>
        </div>
    </div>
</div>
@endsection
