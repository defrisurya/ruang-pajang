@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <h4>Semua Data Transaski Beli</h4>
            </div>
            <div class="card-body">
                <table class="card-table table table-hover text-center">
                    <thead>
                        <tr>
                            <th scope="col">
                                No
                            </th>
                            <th scope="col">
                                Invoice
                            </th>
                            <th scope="col">
                                Tanggal
                                Pembelian
                            </th>
                            <th scope="col">
                                Total Bayar
                            </th>
                            <th scope="col">
                                Status
                            </th>
                            <th scope="col">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @forelse ($data as $item)
                            <tr>
                                <th scope="row">{{ $no++ }}</th>
                                <td>{{ $item->invoice }}</td>
                                <td>{{ Carbon\Carbon::parse($item->tanggalreservasi)->isoFormat('D MMMM Y') }}</td>

                                <td>Rp.{{ number_format($item->totalharga) }}</td>
                                <td>
                                    <div class="status" style="color: red;">
                                        {{ $item->statuspembayaran }}
                                    </div>
                                </td>
                                <td>
                                    <p>
                                        <a class="btn btn-primary" href="{{ route('detailtransaksibeli', $item->id) }}">
                                            Detail
                                        </a>
                                        <a href="https://wa.me/62{{ $item->user->userdetail['telpon'] }}?text=Halo%20{{ $item->user['name'] }}%20Nomer%20Invoice%20Beli%20Lukisan%20Anda%20{{ $item->invoice }}%20Ruang%20Pajang" class="btn btn-warning">
                                            Wa
                                        </a>
                                    </p>
                                </td>
                            </tr>
                            {{-- <tr>
                                <td colspan="6">
                                    <div class="collapse" id="collapseExample">
                                        <div class="card card-body">
                                            <table class="table text-center">
                                                <thead>
                                                  <tr>
                                                    <th scope="col">Foto</th>
                                                    <th scope="col">Dimensi</th>
                                                    <th scope="col">Harga @</th>
                                                    <th scope="col">Seniman</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse ($item->detailbeli  as $row)
                                                        <tr>
                                                            <td>
                                                                <img src="{{ asset($row->foto) }}" class="img-fluid" width="150px" alt="">
                                                            </td>
                                                            <td>{{ $row->dimensi }}</td>
                                                            <td>Rp {{ number_format($row->harga) }}</td>
                                                            <td>{{ $row->seniman }}</td>
                                                        </tr>

                                                    @empty

                                                    @endforelse

                                                </tbody>
                                              </table>
                                        </div>
                                      </div>
                                </td>

                            </tr> --}}
                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
