@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-7 card">
            <div class="card-header">
                <h4>Semua Data Voucher</h4>
            </div>
            <div class="card-body">
                <table class="table text-center table-hover">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Voucher</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Persen</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $item => $voucher)
                            <tr>
                                <th scope="row">{{ $data->firstItem() + $item }}</th>
                                <td>{{ $voucher->namavoucher }}</td>
                                <td>
                                    Rp {{ number_format($voucher->hargavoucher) }}
                                </td>
                                <td>{{ $voucher->persen }} %</td>
                                <td>
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <b><i class="fas fa-ellipsis-v"></i></b>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a href="{{ route('voucher.edit', $voucher) }}" class="dropdown-item">Edit</a>
                                            <form action="{{ route('voucher.destroy', $voucher) }}" method="POST" onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                                {!! method_field('delete') . csrf_field() !!}
                                                <button class="dropdown-item" type="submit">
                                                    Delete
                                                </button>
                                            </form>
                                        </div>
                                </td>
                            </tr>
                        @empty
                            
                        @endforelse
                     
                    </tbody>
                  </table>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12 text-center">
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{ $error }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    @endforeach
                </div>
            </div>
          <div class="card">
            <div class="card-header">
                <h4>Tambah Data Voucher</h4>
            </div>
            <div class="card-body">
             <form action="{{ route('voucher.store') }}" method="POST">
                 @csrf
                 <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Nama Voucher</label>
                    <input type="text" class="form-control" name="namavoucher" placeholder="nama voucher" value="{{ old('namavoucher') }}">
                  </div>

                  <label for="exampleFormControlInput1" class="form-label">Harga</label>
                  <div class="input-group mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-default">Rp</span>
                    <input type="number" name="hargavoucher" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" placeholder="harga voucher" value="{{ old('hargavoucher') }}">
                  </div>

                  <label for="exampleFormControlInput1" class="form-label">Persen</label>
                  <div class="input-group mb-3">
                    <input type="number" name="persen" class="form-control"  placeholder="persen diskon" value="{{ old('persen') }}">
                  </div>

                 <div class="col-md-12 mb-5 text-center">
                     <button class="btn btn-primary">Simpan</button>
                 </div>
             </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
