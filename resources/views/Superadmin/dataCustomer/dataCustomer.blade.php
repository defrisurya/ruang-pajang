@extends('layouts.stisla')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-md-12 card">
            <div class="card-header">
                <div class="container-fluid">
                    <h4>Data Customer</h4>
                    <form class="d-flex col-lg-4 col-md-6" style="float: right" action="{{ URL::current() }}" method="GET">
                        <input class="form-control me-2" placeholder="Cari ..." aria-label="Search" type="text" name="cari" value="{{ request('cari') }}">
                        <button class="btn btn-outline-success" type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="card-body">
                <table class="card-table table table-hover text-center">
                    <thead>
                        <tr>
                            <th scope="col">
                                No
                            </th>
                            <th scope="col">
                                Nama
                            </th>
                            <th scope="col">
                                Email
                            </th>
                            <th scope="col">
                                Status
                            </th>
                            <th scope="col">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @forelse ($data as $item)
                            <tr style="color: black">
                                <th scope="row">{{ $no++ }}</th>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->role }}</td>
                                <td>
                                    <p>
                                        <a class="btn btn-warning" href="{{ route('index.update.customer', $item->id) }}">
                                            Update
                                        </a>
                                    </p>
                                </td>
                            </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>
                {{ $data->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
