@extends('layouts.stisla')

@section('content')
    <div class="main-content">
        <div class="row">
            <div class="col-md-12 card">
                <div class="card-header">
                    <h4>Edit Data Customer</h4>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('update.customer', $data->id) }}" method="POST">
                                @csrf
                                @method('patch')
                                <div class="row">
                                    <div class="form-group col-lg-4 col-md-6">
                                        <label class="form-control-label" for="name">Nama Customer</label>
                                        <input type="text" class="form-control" name="name" style="color: black" value="{{ $data->name }}" disabled>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6">
                                        <label class="form-control-label" for="email">Email Customer</label>
                                        <input type="text" class="form-control" name="email" style="color: black" value="{{ $data->email }}" disabled>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6">
                                        <label class="form-control-label" for="role">Status Customer</label>
                                        <input type="text" class="form-control" name="role" style="color: black" value="{{ $data->role }}" disabled>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6">
                                        <label class="form-control-label" for="telpon">No.Telfon Customer</label>
                                        <input type="text" class="form-control" name="telpon" style="color: black" value="{{ $data->userdetail['telpon'] }}">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6">
                                        <label class="form-control-label" for="latitude">Latitude</label>
                                        <input type="text" class="form-control" name="latitude" style="color: black" value="{{ $data->userdetail['latitude'] }}">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6">
                                        <label class="form-control-label" for="longitude">Longitude</label>
                                        <input type="text" class="form-control" name="longitude" style="color: black" value="{{ $data->userdetail['longitude'] }}">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-6">
                                        <label class="form-control-label" for="longitude">Premium</label>
                                        <select class="form-select" name="premium" aria-label="Default select example">
                                            <option value="">Opsi Fitur</option>
                                            <option {{ ($data->userdetail['premium']) == 'on' ? 'selected' : '' }} value="on">Premium On</option>
                                            <option {{ ($data->userdetail['premium']) == 'off' ? 'selected' : '' }} value="off">Premium Off</option>
                                          </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-4 col-md-6" style="float: right">
                                    <button type="submit" class="btn btn-success" id="simpan">Update</button>
                                    <a class="btn btn-primary" href="{{ route('customer') }}">Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
