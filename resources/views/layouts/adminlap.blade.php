
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Administrasi Lapangan</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('bootstrap') }}/style.css">
    {{-- <link rel="stylesheet" href="{{ asset('bootstrap') }}/fontawsome.css"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- CSS Libraries -->
    {{-- <link rel="stylesheet" href="{{ asset('node_modules') }}/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="{{ asset('node_modules') }}/weathericons/css/weather-icons.min.css">
    <link rel="stylesheet" href="{{ asset('node_modules') }}/weathericons/css/weather-icons-wind.min.css">
    <link rel="stylesheet" href="{{ asset('node_modules') }}/summernote/dist/summernote-bs4.css"> --}}

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/style.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/components.css">
    @yield('css')
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
                <nav class="navbar navbar-expand-lg main-navbar">
                    <form class="form-inline mr-auto">
                        <ul class="navbar-nav mr-3">
                            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                        </ul>
                    </form>
                    <ul class="navbar-nav navbar-right">
                        <div class="navbar-nav ms-auto">
                            <a class="nav-link active d-none d-sm-block" aria-current="page" href="#"><img src="{{ asset('front/admincust') }}/img/person.png" alt=""> {{ auth()->user()->name }}</a>
                        </div>
                    </ul>
                </nav>

                <div class="main-sidebar">
                    <aside id="sidebar-wrapper">
                        <div class="border-right" id="sidebar-wrapper">
                            <div class="sidebar-brand sidebar-heading text-center">
                                <a href="{{ route('adminlap') }}">
                                    <img src="{{ asset('front') }}/img/pajanginlogo.png" width="140" alt="" class="my-4 px-4" />
                                </a>
                            </div>
                        </div>
                        <br><br><br>
                        <ul class="sidebar-menu">
                            <li class="menu-header">Main Menu</li>
                            <li class="nav-item {{ Request::is('AdminLap/dashboard') ? 'active' : '' }}"><a class="nav-link" href="{{ route('adminlap') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
                            <li class="nav-item {{ Request::is('AdminLap/TransaksiBeliMasuk') ? 'active' : '' }}"><a class="nav-link" href="{{ route('belimasuk') }}"><i class="fas fa-fire"></i> <span>Reservasi Beli</span></a></li>
                            <li class="nav-item {{ Request::is('AdminLap/TransaksiSewaMasuk') ? 'active' : '' }}"><a class="nav-link" href="{{ route('sewamasuk') }}"><i class="fas fa-fire"></i> <span>Reservasi Sewa</span></a></li>
                        </ul>
                        <hr>
                        <div class="mt-4 p-3 hide-sidebar-mini">
                            <a class="btn btn-primary btn-lg btn-block btn-icon-split" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt text-danger"></i> Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </aside>
                </div>

    <!-- Main Content -->
    @include('sweetalert::alert')
    @yield('content')


    <footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; 2021 <div class="bullet"></div> INOVASI DIGITAL NUSWANTARA
    </div>
    <div class="footer-right">
        1.0.0
    </div>
    </footer>
</div>
</div>

    <!-- General JS Scripts -->
    <script src="{{ asset('jquery') }}/jquery.js"></script>
    <script src="{{ asset('jquery') }}/pooper.js"></script>
    <script src="{{ asset('jquery') }}/bootstrap.min.js"></script>
    <script src="{{ asset('jquery') }}/jquery.nicescol.min.js"></script>
    <script src="{{ asset('jquery') }}/moment.min.js"></script>
    <script src="{{ asset('assets') }}/js/stisla.js"></script>
    @yield('js')

        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- JS Libraies -->
    {{-- <script src="{{ asset('node_modules') }}/simpleweather/jquery.simpleWeather.min.js"></script>
    <script src="{{ asset('node_modules') }}/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('node_modules') }}/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="{{ asset('node_modules') }}/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="{{ asset('node_modules') }}/summernote/dist/summernote-bs4.js"></script>
    <script src="{{ asset('node_modules') }}/chocolat/dist/js/jquery.chocolat.min.js"></script> --}}

    <!-- Template JS File -->
    <script src="{{ asset('assets') }}/js/scripts.js"></script>
    <script src="{{ asset('assets') }}/js/custom.js"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('assets') }}/js/page/index-0.js"></script>
</body>
</html>
