<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="index.html">Ruang Pajang</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">Rp</a>
      </div>

      <ul class="sidebar-menu">
        <li class="menu-header">Main Menu</li>
        <li class="nav-item {{ Request::is('home') ? 'active' : '' }}"><a class="nav-link" href="{{ route('home') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
        @can('isSuperadmin')
            <li class="nav-item {{ Request::is('Superadmin/kategoriproduk*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('kategoriproduk.index') }}"><i class="fas fa-fire"></i> <span>Kategori Produks</span></a></li>
            <li class="nav-item {{ Request::is('Superadmin/pelukis*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('pelukis.index') }}"><i class="fas fa-fire"></i> <span>Pelukis</span></a></li>
            <li class="nav-item {{ Request::is('Superadmin/produk*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('produk.index') }}"><i class="fas fa-fire"></i> <span>Produks</span></a></li>
            <li class="nav-item {{ Request::is('Superadmin/ongkir*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('ongkir.index') }}"><i class="fas fa-fire"></i> <span>Ongkir</span></a></li>
            <li class="nav-item {{ Request::is('Superadmin/voucher*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('voucher.index') }}"><i class="fas fa-fire"></i> <span>Voucher</span></a></li>
            <li class="nav-item {{ Request::is('Superadmin/TransaksiBeli*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('t.beli.s') }}"><i class="fas fa-fire"></i> <span>Transaksi Beli</span></a></li>
            <li class="nav-item {{ Request::is('Superadmin/TransaksiSewa*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('t.sewa.s') }}"><i class="fas fa-fire"></i> <span>Transaksi Sewa</span></a></li>
            <li class="nav-item {{ Request::is('Superadmin/DataCustomer*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('customer') }}"><i class="fas fa-fire"></i> <span>Data Customer</span></a></li>
        @endcan

        @can('isManajemen')
            <li class="nav-item {{ Request::is('manajemen/MTransaksiBeli*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('m.transaksi.beli') }}"><i class="fas fa-fire"></i> <span>Transaksi Beli</span></a></li>
            <li class="nav-item {{ Request::is('manajemen/MTransaksiSewa*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('m.transaksi.sewa') }}"><i class="fas fa-fire"></i> <span>Transaksi Sewa</span></a></li>
        @endcan
        


      </ul>

        <div class="mt-4 p-3 hide-sidebar-mini">
            <a class="btn btn-primary btn-lg btn-block btn-icon-split" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
             <i class="fas fa-sign-out-alt text-danger"></i> Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </aside>
  </div>
