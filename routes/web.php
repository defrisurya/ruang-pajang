<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'Front\frontController@index')->name('welcome');
Route::get('/artGallery', 'Front\frontController@artGalley')->name('art.gallery');
Route::get('/ourArtist', 'Front\frontController@ourArtist')->name('our.artist');
Route::get('/detailart/{id}', 'Front\frontController@detail')->name('art.detail');
Route::get('/artisdetail/{id}', 'Front\frontController@artisdetail')->name('artis.detail');
Route::get('/aboutus', 'Front\frontController@aboutus')->name('about.us');

Route::get('/filterkategori/{id}', 'Front\frontController@filterkategori')->name('filter.kategori');

Route::post('/keranjangbeli', 'Front\frontController@keranjangbeli')->name('keranjangbeli');
Route::get('/keranjangsewa/{id}', 'Front\frontController@keranjangsewa')->name('keranjangsewa');

// Sewa
// Route::get('/keranjangsewa/{id}', 'Front\transaksiController@tambah_cart')->where('id', '[0-9]+');
// Route::get('/keranjangsewa/hapus/{id}', 'Front\transaksiController@hapus_cart')->where('id', '[0-9]+');
// ends sewa

Route::post('/postregister', 'Front\frontController@postregister')->name('post.register');
// Route::group(['middleware' => ['auth', 'CekRole:customer'], 'prefix' => 'Customer'], function () {
// 	Route::namespace('Front')->group(function () {

// 	});
// });

// Route::get('/registercompany', 'Auth\RegisterController@registercompany')->name('register.company');
// Route::get('/registerpersonal', 'Auth\RegisterController@registerpersonal')->name('register.personal');
// Route::post('postpersonal', 'Auth\RegisterController@postpersonal')->name('post.personal');
// Route::post('postcompany', 'Auth\RegisterController@postcompany')->name('post.company');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/AdminLap/dashboard', 'Front\frontController@adminlap')->name('adminlap');
// Route::get('/Management/dashboard', 'Front\frontController@management')->name('management');


Route::get('/province', 'Front\rajaOngkirController@get_province')->name('province');
Route::get('/kota/{id}', 'Front\rajaOngkirController@get_city')->name('city');
Route::get('/origin={city_origin}&destination={city_destination}&weight={weight}&courier={courier}', 'Front\rajaOngkirController@get_ongkir')->name('ongkir');


// Route::get('/cekkabupaten', 'Front\rajaOngkirController@index');



Route::group(['middleware' => ['auth', 'CekRole:customercompany,customerpersonal']], function () {
    Route::get('/MyDashboardPersonal', 'Front\customerController@index')->name('dashboard.customer');
    Route::get('/MyTransaksiPersonal', 'Front\customerController@tbelipersonal')->name('transaksi.beli.p');
    Route::get('/MyTransaksiSewa', 'Front\customerController@tsewa')->name('transaksi.sewa');
    Route::get('/SettingAkun', 'Front\customerController@settingakunp')->name('setting.akun.p');

    // transaksi
    Route::get('/ListKeranjangBeli', 'Front\transaksiController@listkeranjangbeli')->name('listkeranjangbeli');
    Route::get('/hapuscart/{key}', 'Front\transaksiController@hapuscart')->name('hapus.cart');
    Route::post('/ProsesBeli', 'Front\transaksiController@prosesbeli')->name('proses.orderbeli');

    Route::get('/ListKeranjangSewa', 'Front\transaksiController@listkeranjangsewa')->name('listkeranjangsewa');
    Route::get('/hapuscartsewa/{key}', 'Front\transaksiController@hapus_cart')->name('hapussewa');
    Route::post('/ProsesSewa', 'Front\transaksiController@prosessewa')->name('proses.ordersewa');
});

Route::group(['middleware' => ['auth', 'CekRole:superadmin'], 'prefix' => 'Superadmin'], function () {
    Route::namespace('Superadmin')->group(function () {
        Route::resource('kategoriproduk', 'kategoriProdukController');
        Route::resource('produk', 'produkController');
        Route::resource('pelukis', 'pelukisController')->parameters(['pelukis' => 'pelukis',]);
        Route::resource('ongkir', 'ongkirController');
        Route::resource('voucher', 'voucherController');
        Route::get('TransaksiBeli', 'TransaksiBeli@index')->name('t.beli.s');
        Route::get('DetailTransaksiBeli/{id}', 'TransaksiBeli@DetailBeli')->name('detailtransaksibeli');
        Route::get('KonfirmasiBeli/{id}', 'TransaksiBeli@konfirmasiBeli')->name('konfirmasi.beli');

        Route::get('TolakBeli/{id}', 'TransaksiBeli@TolakBeli')->name('tolakbeli');

        Route::get('TransaksiSewa', 'TransaksiSewa@index')->name('t.sewa.s');
        Route::get('DetailTransaksiSewa/{id}', 'TransaksiSewa@DetailSewa')->name('detailtransaksisewa');
        Route::get('KonfirmasiSewa/{id}', 'TransaksiSewa@konfirmasiSewa')->name('konfirmasi.sewa');
        Route::get('TolakSewa/{id}', 'TransaksiSewa@TolakSewa')->name('tolaksewa');
        Route::get('DataCustomer', 'CustomController@index')->name('customer');
        Route::get('UpdateDataCustomer/{id}', 'CustomController@updateindex')->name('index.update.customer');
        Route::patch('DataCustomer/update/{id}', 'CustomController@update')->name('update.customer');
    });
});

Route::group(['middleware' => ['auth', 'CekRole:adminlap'], 'prefix' => 'AdminLap'], function () {
    Route::namespace('AdminLap')->group(function () {
        Route::get('TransaksiBeliMasuk', 'transaksibelimasuk@BeliMasuk')->name('belimasuk');
        Route::get('KonfirmasiKirimBeli/{id}', 'transaksibelimasuk@KonfirmasiKirimBeli')->name('konfirmasikirimBeli');
        Route::get('TransaksiSewaMasuk', 'transaksisewamasuk@SewaMasuk')->name('sewamasuk');
        Route::get('KonfirmasiKirimSewa/{id}', 'transaksisewamasuk@KonfirmasiKirimSewa')->name('konfirmasikirimSewa');
        
        // Route::get('TransaksiSewa', 'TransaksiSewa@index')->name('t.sewa.s');
        // Route::get('Konfirmasisewa/{id}', 'TransaksiSewa@konfirmasiSewa')->name('konfirmasi.sewa');
    });
});

Route::group(['middleware' => ['auth', 'CekRole:manajemen'], 'prefix' => 'manajemen'], function () {
    Route::namespace('Manajemen')->group(function () {
        Route::get('Manajemen', 'manajemenController@index')->name('manajemen');
        Route::get('MTransaksiBeli', 'manajemenController@transaksibeli')->name('m.transaksi.beli');
        Route::get('MTransaksiBeli/{id}', 'manajemenController@detailtransaksibeli')->name('detail.m.beli');
        Route::get('MTransaksiSewa', 'manajemenController@transaksisewa')->name('m.transaksi.sewa');
        Route::get('MTransaksiSewa/{id}', 'manajemenController@detailtransaksisewa')->name('detail.m.sewa');
    });
});

// run artisan command
Route::get('/artisan', function () {
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('cache:clear');
    return 'OK';
});
